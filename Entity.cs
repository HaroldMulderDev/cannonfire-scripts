﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    [Header("Entity")]
    public int health; // The overal health of the ship
    protected int maxHealth; // The maximum health of the ship
    public Gamemode gamemode;
    public int UID;
    public Profile representingProfile;
    public Vector3 lastPosition;
    public int rankPoints;

    // Start is called before the first frame update
    protected void setup()
    {
        gamemode = Gamemode.instance;
        gamemode.addEntity(this);
        health = gamemode.properties.shipHealth;
        maxHealth = health;
        UID = gamemode.getEntityIndex(this);
        lastPosition = transform.position;
    }

    // Update is called once per frame
    protected void loop()
    {
        if(lastPosition != transform.position)
        {
            float movement = (lastPosition - transform.position).sqrMagnitude*1000;
            gamemode.onEntityMove.Invoke(UID, movement);
            lastPosition = transform.position;
        }
    }

    public void onPlayerAction()
    {
        gamemode.onEntityAction.Invoke(UID);
    }

    public virtual int damage(int amount, int UID)
    {
        if (amount > 0)
        {
            if (UID >= 0)
            {
                gamemode.onDamageDealt.Invoke(UID, amount);
            }
            health -= amount;
            Debug.Log("Taken damage for: " + amount + " health! Remaining: " + health);
            gamemode.onDamageTaken.Invoke(this.UID, amount);
            if(health <= 0)
            {
                gamemode.onDestroyed.Invoke(this.UID);
                if(UID >= 0)
                {
                    gamemode.onDestroyTarget.Invoke(UID);
                }
            }
        }
        return 0;
    }
}