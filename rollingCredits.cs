﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class rollingCredits : MonoBehaviour
{
    [SerializeField] float speed;
    bool last = false;

    public bool isLast { set { last = value; } }
    public UnityEvent onCreditsEnd;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + (Time.deltaTime * speed), transform.position.z);
        if(transform.position.y > 1080)
        {
            if (last)
            {
                onCreditsEnd.Invoke();
            }
            Destroy(this.gameObject);
        }
    }
}
