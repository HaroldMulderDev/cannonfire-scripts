﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Cannonrow : MonoBehaviour
{
    [SerializeField] public GameObject shotEffectPref;
    [SerializeField] Vector3 offset;
    public bool canFire = false;
    public uint Precision; // Determines the behaviour of the shot. 
    public UnityEvent cannonFired;
    public Ship ship;
    [SerializeField] GameObject cannonBallPref;
    [SerializeField] GameObject cannonRow;
    Vector3 cannonRowLocation;

    // Start is called before the first frame update
    void Start()
    {
        cannonRowLocation = new Vector3(0, -0.25f, 0);
        cannonFired.AddListener(onShot);
    }

    // Update is called once per frame
    void Update()
    {
        if (canFire)
        {
            cannonRowLocation = new Vector3(0, cannonRowLocation.y, 0);
        }
        else
        {
            cannonRowLocation = new Vector3(0.5f, cannonRowLocation.y, 0);
        }

        cannonRow.transform.localPosition = Vector3.Lerp(cannonRow.transform.localPosition, cannonRowLocation, 0.15f);
    }

    public void onFire()
    {
        if (canFire)
        {
            //Debug.Log("Starting shooting routine!");
            StartCoroutine(fireOverTime(ship.ShotCount(Precision), ship.AvgShotDelay(Precision)));
            //Debug.Log("delay: " + ship.avgShotDelay);
            //Debug.Log("count: " + ship.shotCount);
        }
    }

    public void onShot()
    {
        GameObject shoteffect = randomShot();
        AudioSource aud = shoteffect.GetComponent<AudioSource>();
        randomizePitch(aud);
    }

    GameObject randomShot()
    {
        GameObject shoteffect = Instantiate(shotEffectPref);
        Vector3[] spawnData = randomPoint();
        shoteffect.transform.position = spawnData[0];
        Quaternion rot = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, -90, 0) + spawnData[1]);
        shoteffect.transform.rotation = rot;

        GameObject cannonball = Instantiate(cannonBallPref);
        cannonball.transform.rotation = rot;
        cannonball.transform.position = spawnData[0] + cannonball.transform.forward;
        cannonball.GetComponent<Cannonball>().origin = ship;
        float spd = ship.ShotSpeed(Precision);
        cannonball.GetComponent<Rigidbody>().AddForce(cannonball.transform.forward*spd + new Vector3(0,spd/3f,0), ForceMode.Impulse);

        return shoteffect;
    }

    void randomizePitch(AudioSource aud)
    {
        float randomizer = Random.Range(-.1f,.5f);
        aud.pitch = aud.pitch + randomizer;
    }

    Vector3[] randomPoint()
    {
        Vector3[] data = new Vector3[2];
        float offset = Random.Range(-1.2f, 1.2f);
        Vector3 pos = transform.position + (transform.forward * offset);
        float angularOffsetter;
        switch (Precision)
        {
            default:
                angularOffsetter = 30;
                break;
            case 1:
                angularOffsetter = 10;
                break;
            case 2:
                angularOffsetter = 1;
                break;
        }
        Vector3 angle = new Vector3(0,offset * angularOffsetter,0);

        data[0] = pos;
        data[1] = angle;

        return data;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IEnumerator fireOverTime(float shotCount, float avgShotDelay)
    {
        //Shoots a shots after a delay, every second shot or the last if uneven ends up on a multiplication of 'delay'
        float totalTime = avgShotDelay * shotCount;
        bool shooting = true;
        int delayIndex = 0;
        List<float> shotDelays = new List<float>();
        while (shotDelays.Count < shotCount)
        {
            if (shotDelays.Count == shotCount - 1)
            {
                // We only have 1 shot left, we just add the average delay
                shotDelays.Add(avgShotDelay);
            }
            else
            {
                // We remove the additional speed from rand 2 to compensate lost time. Both are randomly added to the list
                float rand1, rand2;
                float offset = Random.Range(-avgShotDelay / 2, avgShotDelay / 2);
                rand1 = avgShotDelay + offset;
                rand2 = avgShotDelay - offset;
                shotDelays.Insert((int)Random.Range((int)0, shotDelays.Count - 1), rand1);
                shotDelays.Insert((int)Random.Range((int)0, shotDelays.Count - 1), rand2);
            }
        }

        while (shooting)
        {
            //Debug.Log("firing single shot!");
            cannonFired.Invoke();
            //Debug.Log("Delaying for: " + shotDelays[delayIndex]);
            yield return new WaitForSeconds(shotDelays[delayIndex]);
            delayIndex++;
            if (delayIndex > shotDelays.Count - 1)
            {
                shooting = false;
            }
        }
    }
}
