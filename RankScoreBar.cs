﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankScoreBar : MonoBehaviour
{
    float value = 0;
    [SerializeField] Image bar;
    [SerializeField] Text displayAmount;
    [SerializeField] Text rankBanner;
    [SerializeField] Color defaultColor;
    [SerializeField] string defaultRank;
    [SerializeField] Color[] colors;
    [SerializeField] int[] tiers;
    [SerializeField] string[] ranks;
    bool validSetup = true;

    public float setValue{
        set {
            this.value = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (tiers.Length != colors.Length || tiers.Length != ranks.Length)
        {
            Debug.LogWarning("Color tiers setup incorrectly!");
            validSetup = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!validSetup)
        {
            return;
        }

        //value += Time.deltaTime*150;
        value = Mathf.Clamp(value, 0, 1500);
        bar.fillAmount = Mathemetics.toExponential(value/1500, 1);

        displayAmount.text = Mathf.RoundToInt(value).ToString() + "\n" + "CP";

        Color fillColor = defaultColor;
        string rankText = defaultRank;
        for(int i = 0; i < tiers.Length; i++)
        {
            if(value > tiers[i])
            {
                fillColor = colors[i];
                rankText = ranks[i];
            }
        }
        bar.color = fillColor;
        displayAmount.color = fillColor;
        rankBanner.color = fillColor;
        rankBanner.text = rankText;
    }
}
