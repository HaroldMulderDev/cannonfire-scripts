﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkinLayer : MonoBehaviour
{
    public Material skinTexture;
    [SerializeField] SkinLayer provider;
    public UnityEvent onSkinChange;

    // Start is called before the first frame update
    void Start()
    {
        if(provider != null)
        {
            provider.onSkinChange.AddListener(setupSkin);
        }
    }

    public void setupSkin()
    {
        if (provider == null)
        {
            updateSkin(skinTexture);
        }
        else
        {
            updateSkin(provider.skinTexture);
        }
    }

    public void updateSkin(Material mat)
    {
        onSkinChange.Invoke();
        Renderer renderer = this.GetComponent<Renderer>();
        if(renderer != null)
        {
            renderer.material = mat;
        }
    } 

    // Update is called once per frame
    void Update()
    {
        
    }
}
