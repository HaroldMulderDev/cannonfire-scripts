﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class statTracker : MonoBehaviour
{
    bool firstUpdate = false;

    [Header("Selected")]
    [SerializeField] public int statType;
    [SerializeField] Entity entity;
    [SerializeField] Image icon;
    [SerializeField] Text stat;
    [SerializeField] Sprite[] icons;

    [Header("Dynamic")]
    public int entityID;
    public Gamemode gamemode;
    public GameStatistics stats;

    // Start is called before the first frame update
    void Start()
    {
        gamemode = Gamemanager.Instance.gamemode;
        stats = gamemode.stats;
        icon.sprite = icons[statType];
        gamemode.AddTracker(this);
        switch (statType)
        {
            case 0:
                //damage dealt
                stats.onDamageUpdate.AddListener(this.updateTracker);
                break;
            case 1:
                //Kills made
                stats.onEntityDestroyTarget.AddListener(this.updateTracker);
                break;
            case 2:
                //distance moved
                stats.onEntityMoving.AddListener(this.updateTracker);
                break;
            case 3:
                //rank points
                stats.onRankPointIncrease.AddListener(this.updateTracker);
                break;
        }
    }

    public void updateTracker()
    {
        switch (statType)
        {
            case 0:
                if (stats.damageDealt.ContainsKey(entityID))
                {
                    stat.text = stats.damageDealt[entityID].ToString();
                }
                else
                {
                    stat.text = "";
                }
                break;
            case 1:
                if (stats.killCount.ContainsKey(entityID))
                {
                    stat.text = stats.killCount[entityID].ToString();
                }
                else
                {
                    stat.text = "";
                }
                break;
            case 2:
                if (stats.movement.ContainsKey(entityID))
                {
                    stat.text = stats.movement[entityID].ToString();
                }
                else
                {
                    stat.text = "";
                }
                break;
            case 3:
                if (stats.rankPointsByEntity.ContainsKey(entityID))
                {
                    stat.text = stats.rankPointsByEntity[entityID].ToString();
                }
                else
                {
                    stat.text = "";
                }
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!firstUpdate)
        {
            firstUpdate = true;
            entityID = gamemode.getEntityIndex(entity);
        }
    }
}
