﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodePather : MonoBehaviour
{
    Pathmode currentPathmode;
    Vector3 startPos;
    Quaternion startRot;
    GameObject targetNode;
    float moveStep = 0;
    float moveProgress = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(targetNode != null)
        {
            if(moveProgress == 1)
            {
                targetNode = null;
            }
            else
            {
                moveProgress += moveStep * Time.deltaTime;
                //Debug.Log("Progressing to node: " + moveProgress * 100 + "%");
                moveProgress = Mathf.Clamp(moveProgress, 0, 1);
                currentPathmode.move(startPos, startRot, targetNode.transform, this.transform, moveProgress);
            }
        }
    }

    /// <summary>
    /// Moves to the given node in the given amount of time
    /// </summary>
    /// <param name="node">The node to move to</param>

    public void goToNode(GameObject node)
    {
        goToNode(node, .5f, Pathmode.moveAndRotate);
    }

    /// <summary>
    /// Moves to the given node in the given amount of time
    /// </summary>
    /// <param name="node">The node to move to</param>

    public void goToNode(GameObject node, float time)
    {
        goToNode(node, time, Pathmode.moveAndRotate);
    }

    /// <summary>
    /// Moves to the given node in the given amount of time
    /// </summary>
    /// <param name="node">The node to move to</param>
    /// <param name="time">The time over which the move happens</param>
    /// <param name="pathmode">The pathmode to use</param>
    public void goToNode(GameObject node, float time, Pathmode pathmode)
    {
        targetNode = node;
        startPos = transform.position;
        startRot = transform.rotation;
        currentPathmode = pathmode;
        moveStep = 1/time;
        moveProgress = 0;
    }

    
}
