﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skin : MonoBehaviour
{
    [SerializeField] Material material;
    [SerializeField] GameObject passiveEffectPref;
    [SerializeField] GameObject shotEffectPref;
    [SerializeField] GameObject impactEffectPref;
    [SerializeField] GameObject rammingEffectPref;
    [SerializeField] GameObject rammingImpactEffectPref;

    public Material Material {
        get { return material; }
    }

    public GameObject PassiveEffect {
        get { return passiveEffectPref; }
    }

    public GameObject ShotEffect {
        get { return shotEffectPref; }
    }

    public GameObject ImpactEffect {
        get { return impactEffectPref; }
    }

    public GameObject RamEffect {
        get { return rammingEffectPref; }
    }

    public GameObject RamImpactEffect {
        get { return rammingImpactEffectPref; }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
