﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    [SerializeField] AudioClip[] tracks;
    AudioClip[] orderedTracks;
    int current = 0;
    AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        source = this.GetComponent<AudioSource>();
        orderedTracks = shuffle(tracks);
        playTrack(orderedTracks[current]);
    }

    // Update is called once per frame
    void Update()
    {
        if (!this.source.isPlaying)
        {
            if(current > orderedTracks.Length)
            {
                current = 0;
                orderedTracks = shuffle(tracks);
            }
            else
            {
                current++;
            }

            playTrack(orderedTracks[current]);
        }
    }

    /// <summary>
    /// Shuffles the audio list and returns the shuffled copy
    /// </summary>
    /// <param name="list">The original list of AudioClips</param>
    /// <returns>The shuffled list</returns>
    AudioClip[] shuffle(AudioClip[] list)
    {
        AudioClip[] shuffledList = new AudioClip[tracks.Length];
        foreach(AudioClip a in list)
        {
            bool slotFound = false;
            while (!slotFound)
            {
                int i = Random.Range(0, tracks.Length);
                if(shuffledList[i] == null)
                {
                    shuffledList[i] = a;
                    slotFound = true;
                }
            }
        }
        return shuffledList;
    }

    void playTrack(AudioClip sound)
    {
        source.clip = sound;
        source.Play();
    }
}
