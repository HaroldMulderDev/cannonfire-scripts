﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleSound : MonoBehaviour
{
    AudioSource aud;
    [SerializeField] bool destroyOnFinish = false;

    // Start is called before the first frame update
    void Start()
    {
        aud = this.GetComponent<AudioSource>();
        aud.Play(); 
    }

    // Update is called once per frame
    void Update()
    {
        if (destroyOnFinish && !aud.isPlaying)
        {
            Destroy(this.gameObject);
        }
    }
}
