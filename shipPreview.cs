﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shipPreview : MonoBehaviour
{
    [SerializeField] bool hasProfilePreview = true;
    [SerializeField] Profile profileToPreview;
    [SerializeField] int indexOfPreviewProfile;
    SkinLayer skinLayer;
    bool started = false;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(lateStart(1));
    }

    IEnumerator lateStart(float time)
    {
        yield return new WaitForSeconds(time);
        skinLayer = GetComponent<SkinLayer>();
        if (hasProfilePreview)
        {
            profileToPreview = Gamemanager.Instance.profiles[indexOfPreviewProfile];
            profileToPreview.onSkinUpdate.AddListener(onUpdatePreview);
        }
        else
        {
            skinLayer.setupSkin();
        }
        started = true;
    }

    void onUpdatePreview()
    {
        if (!started)
        {
            return;
        }
        Debug.Log("heyo!");
        GameObject skinData = profileToPreview.CurrentSkin;
        Skin skin = skinData.GetComponent<Skin>();
        skinLayer.skinTexture = skin.Material;
        skinLayer.setupSkin();
    }
}
