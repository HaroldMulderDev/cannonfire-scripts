﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDWorldAnchor : MonoBehaviour
{
    [SerializeField] GameObject worldAnchor; // The game object to position relative to
    [SerializeField] Vector3 HUDAnchor; // The offset from the gameobject on the HUD
    Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = cam.WorldToScreenPoint(worldAnchor.transform.position) + HUDAnchor;
        transform.position = pos;
    }
}
