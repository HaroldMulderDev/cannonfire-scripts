﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Ship : Entity
{
    [Header("Components")]
    [SerializeField] List<Cannonrow> cannons;
    AdvancedInput advancedInput;
    [SerializeField] GameObject callbackPref;
    [SerializeField] RammingNode rammingNode;
    [SerializeField] GameObject mast;
    GameObject skinDataPref;
    [SerializeField] SkinLayer skinlayer;
    Rigidbody rb;
    [Header("General")]
    [SerializeField] float fullChargeTime; // The time in seconds before the crew is fully charged
    [SerializeField] float fullChargeDelay; // The time in seconds before this object starts charing after using charge.
    float chargeDelay;
    float chargeRate;
    [Header("Movement")]
    [SerializeField] float speed; // The overal speed of the ship, Based on knots
    [SerializeField] float torque; // The rotational speed of the ship
    [SerializeField] bool wasd; // Wether this ship uses WASD input
    [SerializeField] float rapidTurnCost; // The passive cost of doing a quick turn per second
    [SerializeField] float impactDragMultiplier; // The amount this ship is slowed when it gets hit by something
    [SerializeField] float bounceDelay; // The time in secondsbefore the ship can bounce of an island again
    float bounceTimer;
    float temporaryDrag; // The time in seconds for which every type of movement of the ship is significantly slowed
    float standardDrag; // The amount of drag the rigidbody recieves by default.
    [Header("Cannons")]
    [SerializeField] public int shotCount; // The amount of shots per burst
    [SerializeField] public float avgShotDelay; // The average time per shot, actual delay may differ
    [SerializeField] public float[] shotPrecisionSteps; // The time it takes for the cannons to be 1 step more precise
    [SerializeField] float[] shotCosts;
    [SerializeField] float minimumShotDelay; // The time in seconds until a ship can fire again
    float shotTimer;
    [Header("Ramming")]
    [SerializeField] float boostDelay; // The time in seconds between acceleration boosts
    [SerializeField] float boost; // The amount of speed that is added
    [SerializeField] float boostChargeCost; // The cost for the heavy acceleration from a boost
    [SerializeField] float passiveBoostChargeCost; // The passive cost for holding down the boost button even when no boost is recieved
    [SerializeField] float rammingThreshold; // The required speed before another ship can be rammed
    [SerializeField] GameObject ramSpeedEffectPref; // The prefab for the ramming speed effect
    [SerializeField] float rammingSpeedEffectDelay; // The delay in second between playing the ramming speed effect
    float rammingSpeedEffectTimer;
    float boostTimer;
    [Header("HUD")]
    [SerializeField] GameObject[] ShotMarkers; // The gameobject for the shot marking element
    [SerializeField] Slider healthbar; // The big health bar at the top of the screen
    [SerializeField] Image crewEnergyBar; // The circular bar indicating crew energy
    Color color = new Color(1, 0.4f, 0.1f);
    [Header("Ranking")]
    [SerializeField] int[] healthTiers;
    public int rankWorth = 1;
    [Header("Effects")]
    [SerializeField] GameObject BoostEffectPref; // The effect with sound for boosting with a ship
    [SerializeField] GameObject impactEffectPref;
    [SerializeField] GameObject rammingImpactEffectPref;
    [SerializeField] GameObject PassiveEffectPref;

    float shotHoldTime; // The amount of time the user has held the shooting key for
    float stunTime; // The amount of tiem for which this ship cannot move on its own.
    float charge = 1; // The current charge of the crew, used for firing cannons and ramming
    Curve speedMult; // A multiplier to speed making acceleration happen faster or slower
    Curve rotationMult; // A multiplier to rotation making turning go faster or slower
    float sqrdMaxRigidBodySpeed = 15; // The average max speed estimate used for calculating speed from max
    float speedOfMax = 0; // Value between 0 and 1 indicating how far towards max speed we are. 1 Indicates full speed 0
    List<KeyCode> keys; // the input key list
    List<GameObject> shotMarkers = new List<GameObject>(); // an active list of shotmarkers
    float startingHeight;

    UnityEvent cannonReleased = new UnityEvent();
    #region Setup

    // Start is called before the first frame update
    void Start()
    {
        setup();
        startingHeight = transform.position.y;
        chargeRate = (float)1 / fullChargeTime;
        if (wasd)
        {
            color = Color.blue;
        }
        keys = new List<KeyCode>();

        foreach (Cannonrow c in cannons)
        {
            c.ship = this;
            cannonReleased.AddListener(c.onFire);
        }
        if(cannons.Count > 1)
        {
            cannons[0].canFire = true;
        }

        if (wasd)
        {
            keys.Add(KeyCode.W);
            keys.Add(KeyCode.S);
            keys.Add(KeyCode.A);
            keys.Add(KeyCode.D);
            keys.Add(KeyCode.Space);
        }
        else
        {
            keys.Add(KeyCode.UpArrow);
            keys.Add(KeyCode.DownArrow);
            keys.Add(KeyCode.LeftArrow);
            keys.Add(KeyCode.RightArrow);
            keys.Add(KeyCode.RightControl);
        }

        speedMult = new Curve(8, 13);
        rotationMult = new Curve(1, 0.4f);

        rb = this.GetComponent<Rigidbody>();
        advancedInput = new AdvancedInput(callbackPref);
        standardDrag = rb.drag;

        advancedInput.createCallback("doubleForward", Callback.doubleTap, keys[0]);
        advancedInput.subscribe("doubleForward", onDoubleForward);
        advancedInput.createCallback("doubleBackward", Callback.doubleTapDown, keys[1]);
        advancedInput.subscribe("doubleBackward", onDoubleBackward);
        advancedInput.createCallback("doubleLeft", Callback.doubleTap, keys[2]);
        advancedInput.subscribe("doubleLeft", onDoubleLeft);
        advancedInput.createCallback("doubleRight", Callback.doubleTap, keys[3]);
        advancedInput.subscribe("doubleRight", onDoubleRight);

        StartCoroutine(lateStart(1));
    }

    IEnumerator lateStart(float time)
    {
        yield return new WaitForSeconds(time);
        skinDataPref = representingProfile.CurrentSkin;
        if(skinDataPref == null)
        {
            skinDataPref = Gamemanager.Instance.skinManager.getSkin("default");
        }
        applySkinData();
        representingProfile.onSkinUpdate.AddListener(updateSkin);
    }

    void applySkinData()
    {
        Skin skin = skinDataPref.GetComponent<Skin>();
        skinlayer.skinTexture = skin.Material;
        skinlayer.setupSkin();

        if (skin.PassiveEffect != null)
        {
            Instantiate(skin.PassiveEffect, this.transform);
        }
        BoostEffectPref = skin.RamEffect;
        impactEffectPref = skin.ImpactEffect;
        rammingImpactEffectPref = skin.RamImpactEffect;
        PassiveEffectPref = skin.PassiveEffect;

        foreach(Cannonrow c in cannons)
        {
            c.shotEffectPref = skin.ShotEffect;
        }
    }
    #endregion
    #region Update Loops

    // Update is called once per frame
    void Update()
    {
        loop();
        if (health <= 0)
        {
            transform.position = transform.position - (new Vector3(0, 0.5f, 0) * Time.deltaTime);
            healthbar.GetComponent<HealthBar>().empty();
            crewEnergyBar.enabled = false;
            for (int i = shotMarkers.Count - 1; i >= 0; i--)
            {
                Destroy(shotMarkers[i]);
            }
            shotMarkers.Clear();
            return;
        }
        if (boostTimer > 0)
        {
            boostTimer -= Time.deltaTime;
        }
        if(rammingSpeedEffectTimer > 0)
        {
            rammingSpeedEffectTimer -= Time.deltaTime;
        }
        if (shotTimer > 0)
        {
            shotTimer -= Time.deltaTime;
        }
        if(temporaryDrag > 0)
        {
            temporaryDrag -= Time.deltaTime;
            //Debug.Log("Ship slowed for: " + temporaryDrag);
        }

        if (chargeDelay > 0)
        {
            chargeDelay -= Time.deltaTime;
        }
        else
        {
            charge += chargeRate * Time.deltaTime;
        }
        if(bounceTimer > 0)
        {
            bounceTimer -= Time.deltaTime;
        }
        charge = Mathf.Clamp(charge, 0, 1);
        crewEnergyBar.fillAmount = charge;

        float sqrVelocity = rb.velocity.sqrMagnitude;
        //Debug.Log(sqrVelocity);

        for (int i = shotMarkers.Count - 1; i >= 0; i--)
        {
            Destroy(shotMarkers[i]);
        }
        shotMarkers.Clear();

        if (sqrVelocity == 0)
        {
            speedOfMax = 1;
        }
        else
        {
            //Debug.Log(maxRigidBodySpeed / sqrVelocity);
            speedOfMax = (sqrVelocity / sqrdMaxRigidBodySpeed);
            speedOfMax = Mathf.Clamp(speedOfMax, 0f, 1f);
        }

        float currentSpeedMultiplier = speedMult.getValue(speedOfMax);
        float currentRotationMultiplier = rotationMult.getValue(speedOfMax);

        //Debug.Log(speedFromMax);
        if (stunTime <= 0)
        {
            if (Input.GetKey(keys[0]))
            {
                Vector3 force = transform.forward * speed * currentSpeedMultiplier * Time.deltaTime;
                rb.AddForce(force, ForceMode.Acceleration);
            }
            else if(Input.GetKey(keys[1]))
            {
                Vector3 force = -transform.forward * speed/20 * currentSpeedMultiplier * Time.deltaTime;
                rb.AddForce(force, ForceMode.Acceleration);
            }
        }
        else
        {
            stunTime -= Time.deltaTime;
        }

        if (Input.GetKey(keys[2]))
        {
            Vector3 torque = -transform.up * this.torque * currentRotationMultiplier * Time.deltaTime;
            rb.AddTorque(torque, ForceMode.Acceleration);
        }

        if (Input.GetKey(keys[3]))
        {
            Vector3 torque = transform.up * this.torque * currentRotationMultiplier * Time.deltaTime;
            rb.AddTorque(torque, ForceMode.Acceleration);
        }

        if (Input.GetKey(keys[4]))
        {
            shotHoldTime += Time.deltaTime;
            showShotMarker();
        }

        if(Input.GetKeyUp(keys[4]) && charge >= shotCosts[0])
        {
            uint precision = 0;
            if (shotHoldTime > shotPrecisionSteps[0] && charge >= shotCosts[1])
            {
                precision = 1;
            }
            if (shotHoldTime > shotPrecisionSteps[1] && charge >= shotCosts[2])
            {
                precision = 2;
            }

            float chargeCost = 0;

            switch (precision)
            {
                default:
                    chargeCost = shotCosts[0];
                    break;

                case 1:
                    chargeCost = shotCosts[1];
                    break;

                case 2:
                    chargeCost = shotCosts[2];
                    break;
            }

            if(charge >= chargeCost && shotTimer <= 0) {
                charge -= chargeCost;
                chargeDelay = fullChargeDelay;
                shotTimer = minimumShotDelay;

                foreach (Cannonrow c in cannons)
                {
                    c.Precision = precision;

                }

                cannonReleased.Invoke();
            }

            resetHoldTime();
            //SwapCannons();
        }

        if (temporaryDrag > 0)
        {
            rb.drag = standardDrag * impactDragMultiplier;
        }
        else
        {
            rb.drag = standardDrag;
        }

        if (bounceTimer <= 0 && gamemode.properties.doShipBouncing)
        {
            LayerMask mask = LayerMask.GetMask("Islands", "Invisible");
            Vector3 edge = transform.right * 0.7f;
            if (Physics.Raycast(transform.position + edge, transform.forward, 2.5f, mask) || Physics.Raycast(transform.position - edge, transform.forward, 2.5f, mask))
            {
                Vector3 force = -transform.forward * (rb.velocity.magnitude * 2);
                rb.AddForce(force, ForceMode.Impulse);
                bounceTimer = bounceDelay;
            }
        }

        checkPlayerInputs();
        updateRammingBehaviour();
        updateHealthBar();
        updateMastRotation();
        lockYHeight();
    }

    void checkPlayerInputs()
    {
        foreach(KeyCode key in keys)
        {
            if (Input.GetKeyDown(key))
            {
                onPlayerAction();
            }
        }
    }

    void updateRammingBehaviour()
    {
       //Debug.Log(rb.velocity.sqrMagnitude);
        if (rb.velocity.sqrMagnitude > rammingThreshold)
        {
            if(rammingSpeedEffectTimer <=0)
            {
                rammingSpeedEffectTimer = rammingSpeedEffectDelay;
                GameObject go = Instantiate(ramSpeedEffectPref);
                go.transform.position = this.transform.position + (rb.velocity.normalized*3);
            }
            //allow ramming
            //Debug.Log("Ramming Speed Achieved!");
            rammingNode.canRam = true;
        }
        else
        {
            rammingNode.canRam = false;
        }
    }
    
    void updateHealthBar()
    {
        float percent = (float)health / (float)maxHealth;
        healthbar.value = percent;
        //Debug.Log("healthbar at: " + percent*100 + "%");
    }

    void updateMastRotation()
    {
        float angularLean = rb.angularVelocity.y * 8;
        this.transform.localRotation = Quaternion.Euler(new Vector3(0, transform.localRotation.eulerAngles.y, angularLean));
        mast.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, (rb.angularVelocity.y*25)+90, 0));
    }

    void lockYHeight()
    {
        transform.position = new Vector3(transform.position.x, startingHeight, transform.position.z);
    }

    #endregion
    #region Function

    void SwapCannons()
    {
        if (cannons[0].canFire)
        {
            cannons[0].canFire = false;
            cannons[1].canFire = true;
        }
        else if (cannons[1].canFire)
        {
            cannons[1].canFire = false;
            cannons[0].canFire = true;
        }
    }

    public void drainCharge()
    {
        chargeDelay = fullChargeDelay;
    }

    void showShotMarker() {
        int index = 0;
        if (shotHoldTime > shotPrecisionSteps[0] && charge >= shotCosts[1])
        {
            index = 1;   
        }
        if (shotHoldTime > shotPrecisionSteps[1] && charge >= shotCosts[2])
        {
            index = 2;
        }

        Color c = color;
        if(charge < shotCosts[index] || shotTimer > 0)
        {
            c = Color.red;
        }

        if (cannons[0].canFire)
        {
            GameObject go = Instantiate(ShotMarkers[index]);
            go.transform.position = this.transform.position + (-this.transform.right * 3) + new Vector3(0,.5f,.5f);
            Vector3 RotaryOffset = new Vector3(0, this.transform.rotation.eulerAngles.y, 0);
            Vector3 rotStart = RotaryOffset;
            go.transform.rotation = Quaternion.Euler(rotStart + new Vector3(0,90,0));
            go.GetComponent<ShotMarker>().color = c;
            shotMarkers.Add(go);
        }
        if (cannons[1].canFire)
        {
            GameObject go = Instantiate(ShotMarkers[index]);
            go.transform.position = this.transform.position + (this.transform.right * 3) + new Vector3(0, .5f, 0);
            Vector3 RotaryOffset = new Vector3(0, this.transform.rotation.eulerAngles.y, 0);
            Vector3 rotStart = RotaryOffset;
            go.transform.rotation = Quaternion.Euler(rotStart + new Vector3(0, -90, 0));
            go.GetComponent<ShotMarker>().color = c;
            shotMarkers.Add(go);
        }
    }

    void resetHoldTime()
    {
        shotHoldTime = 0;
    }

    public override int damage(int amount, int UID)
    {
        int tierDestructionCount = 0;
        if(amount > 0)
        {
            int healthBeforeHit = health;
            base.damage(amount, UID);
            foreach (int tier in healthTiers)
            {
                if (healthBeforeHit >= tier && health < tier)
                {
                    tierDestructionCount++;
                }
            }

            //Debug.Log("Current Health: " + health);
            //rb.AddForce(-transform.forward*0.6f, ForceMode.Impulse);
            temporaryDrag += amount / 250;
        }
        return tierDestructionCount;
    }

    void updateSkin()
    {
        skinDataPref = representingProfile.CurrentSkin;
        applySkinData();
    }

    #endregion
    #region Advanced Getters/Setters

    public int ShotCount(uint precision)
    {
        switch (precision)
        {
            default:
                return shotCount;
                break;
            case 1:
                return shotCount + 4;
                break;
            case 2:
                return shotCount + 12;
                break;
        }
    }

    public float AvgShotDelay(uint precision)
    {
        switch (precision)
        {
            default:
                return avgShotDelay - 0.06f;
                break;
            case 1:
                return avgShotDelay - 0.04f;
                break;
            case 2:
                return avgShotDelay;
                break;
        }
    }

    public float ShotSpeed(uint precision)
    {
        switch (precision)
        {
            default:
                return 0.40f;
                break;
            case 1:
                return 0.50f;
                break;
            case 2:
                return 0.70f;
                break;
        }
    }

    public float activeSpeed {
        get { return rb.velocity.sqrMagnitude; }
    }

    #endregion
    #region Controls
    void onDoubleForward()
    {
        charge -= passiveBoostChargeCost * Time.deltaTime;
        chargeDelay = fullChargeDelay;
        // Ramming behaviour
        if (boostTimer <= 0 && charge >= boostChargeCost)
        {
            // boost
            Vector3 force = transform.forward * boost;
            rb.AddForce(force, ForceMode.Impulse);
            GameObject go = Instantiate(BoostEffectPref);
            go.transform.position = this.transform.position;
            charge -= boostChargeCost;
            boostTimer = boostDelay;
        }
        else if(charge < boostChargeCost)
        {
            advancedInput.cancel("doubleForward");
        }
    }


    void onDoubleBackward()
    {
        SwapCannons();
    }

    void onDoubleLeft()
    {
        if(charge >= rapidTurnCost * Time.deltaTime)
        {
            Vector3 torque = -transform.up * (this.torque / 3) * rb.velocity.magnitude * Time.deltaTime;
            rb.AddTorque(torque, ForceMode.Acceleration);
            if (rapidTurnCost > 0)
            {
                charge -= rapidTurnCost * Time.deltaTime;
                chargeDelay = fullChargeDelay;

            }
        }
    }

    void onDoubleRight()
    {
        if (charge >= rapidTurnCost * Time.deltaTime)
        {
            Vector3 torque = transform.up * (this.torque / 3) * rb.velocity.magnitude * Time.deltaTime;
            rb.AddTorque(torque, ForceMode.Acceleration);
            if (rapidTurnCost > 0)
            {
                charge -= rapidTurnCost * Time.deltaTime;
                chargeDelay = fullChargeDelay;
            }
        }
    }

    public void addCharge(float charge)
    {
        this.charge += charge;
        this.charge = Mathf.Clamp(this.charge, 0, 1);
    }
    #endregion
}