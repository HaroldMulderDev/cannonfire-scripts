﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class VolumeSlider : MonoBehaviour
{
    [SerializeField] string audioGroup;
    [SerializeField] Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        Audiomanager aud = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<Audiomanager>();
        switch (audioGroup)
        {
            case "Master":
                slider.value = aud.getMasterVolume(true);
                break;
            case "Music":
                slider.value = aud.getMusicVolume(true);
                break;
            case "SFX":
                slider.value = aud.getSFXVolume(true);
                break;
            case "Ambient":
                slider.value = aud.getAmbientVolume(true);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
