﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{
    [SerializeField] GameObject creditsTextPref;
    [SerializeField] float spacing;
    string[] credits;
    float position;
    float topPos;
    float horizontalPos;
    

    // Start is called before the first frame update
    void Start()
    {
        RectTransform rct = this.GetComponent<RectTransform>();
        topPos = rct.rect.height/2;
        horizontalPos = rct.rect.width/4;
        credits = (JSONInterface.readFile("credits.csv", "", false).Split('\n'));
        for (int i = 0; i < credits.Length; i++)
        {
            credits[i] = credits[i].Replace(';', '\n');

            GameObject gameObject = Instantiate(creditsTextPref, this.transform);
            gameObject.transform.position = new Vector3(gameObject.transform.position.x - horizontalPos, gameObject.transform.position.y - (spacing * i) - topPos, gameObject.transform.position.z);
            gameObject.GetComponent<Text>().text = credits[i];

            if(i == credits.Length - 1)
            {
                // Last credit object
                gameObject.GetComponent<rollingCredits>().isLast = true;
                gameObject.GetComponent<rollingCredits>().onCreditsEnd.AddListener(exitScene);
            }
        }
    }

    void exitScene()
    {
        SceneManager.LoadScene("UI", LoadSceneMode.Single);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            exitScene();
        }
    }
}
