﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct Point
{
    public float position;
    public float value;
    public string Name;
}

/// <summary>
/// A curve of points. Curves allow for linear values between points to be calculated.
/// Use addPoint to get started.
/// </summary>
public class Curve
{
    List<Point> points;

    /// <summary>
    /// Curve constructor, Adds two points by default
    /// </summary>
    /// <param name="min">The value at the start of the curve</param>
    /// <param name="max">The value at the end of the curve</param>
    public Curve(float min = 0, float max = 0)
    {
        points = new List<Point>();

        Point minPoint = new Point();
        minPoint.position = 0;
        minPoint.value = min;
        minPoint.Name = "min";
        points.Add(minPoint);

        Point maxPoint = new Point();
        maxPoint.position = 1;
        maxPoint.value = max;
        maxPoint.Name = "max";
        points.Add(maxPoint);
    }

    /// <summary>
    /// Adds a new point to the curve
    /// </summary>
    /// <param name="position">The position between 0-1 on the curve</param>
    /// <param name="value">The value the point should return</param>
    /// <param name="name">An identifier name for the point</param>
    /// <returns>True if point could be added</returns>
    public bool addPoint(float position, float value, string name)
    {
        if(position < 0 || position > 1)
        {
            return false;
        }

        int index = 0;
        foreach (Point p in points) {
            if (p.Name == name)
            {
                return false;
            }
            if (position > p.position)
            {
                index++;
            }
            else if (position == p.position)
            {
                return false;
            }
        }

        Point point = new Point();
        point.position = position;
        point.value = value;
        point.Name = name;
        points.Insert(index, point);
        return true;
    }

    /// <summary>
    /// Removes a point from the curve
    /// </summary>
    /// <param name="name">The name of the point to remove</param>
    /// <returns>True if points was found and removed</returns>
    public bool removePoint(string name)
    {
        foreach (Point p in points)
        {
            if (p.Name == name)
            {
                points.Remove(p);
                return true;
            }
        }
        return false;
    }


    public float getValue(float position)
    {
        // Can we check?
        if (points.Count < 2)
        {
            Debug.LogWarning("Cannot get value with less than 1 point");
            return 0;
        }

        // Check for the two neighbour points
        int ln = -1;
        int rn = -1;

        if (position == 1){
            return points[points.Count - 1].value;
        }

        for(int i = 0; i < points.Count; i++)
        {
            if (points[i].position > position)
            {
                rn = i;
                ln = i - 1;
                //Debug.Log("rn.index =" + rn);
                //Debug.Log("ln.index =" + ln);
                break;
            }
        }
        if (ln == -1 || rn == -1)
        {
            Debug.LogWarning("Attempt to check point out of range");
            return -0;
        }

        // Check if the left of them is equal on position, if so, return its value
        if(position == points[ln].position)
        {
            return points[ln].value;
        }

        // Check where in between the two points we are with a new 0-1 value
        float initial = points[ln].value;
        float fullAdditional = points[rn].value - points[ln].value;
        float inBetween = (position-points[ln].position) / (points[rn].position-points[ln].position);

        // Return a value equal to (left neighbour) + ln-rn * inBetween
        float additional = fullAdditional * inBetween;
        return initial + additional;
    }

    /// <summary>
    /// Returns the valeu of a named point
    /// </summary>
    /// <param name="name">The name of the point</param>
    /// <returns>The value of the point, 0 if it isn't found</returns>
    public float getValue(string name)
    {
        foreach(Point p in points)
        {
            if(p.Name == name)
            {
                return p.value;
            }
        }
        Debug.LogWarning("No point with this name exists within the curve!");
        return 0;
    }
}
