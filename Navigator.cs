﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Navigator : MonoBehaviour
{
    EventSystem system;

    // Start is called before the first frame update
    void Start()
    {
        system = this.GetComponent<EventSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void navigateTo(GameObject selected)
    {
        system.SetSelectedGameObject(selected);
    }

    public void clearSelected()
    {
        system.SetSelectedGameObject(null);
    }
}
