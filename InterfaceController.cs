﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceController : MonoBehaviour
{
    Canvas canvas;

    // Start is called before the first frame update
    void Start()
    {
        canvas = this.gameObject.GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void disableSelf()
    {
        canvas.enabled = false;
    }

    public void enableSelf()
    {
        canvas.enabled = true;
    }

    public void quitApp()
    {
        Application.Quit();
    }
}
