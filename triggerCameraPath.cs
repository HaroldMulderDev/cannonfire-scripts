﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerCameraPath : MonoBehaviour
{
    [SerializeField] GameObject targetNode;
    [SerializeField] NodePather cam;

    // Start is called before the first frame update
    void Start()
    {
        if (cam == null)
        {
            cam = Camera.current.gameObject.GetComponent<NodePather>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onActivate()
    {
        cam.goToNode(targetNode);
    }
}
