﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect_Waving : MonoBehaviour
{
    [SerializeField] float Intensity; // The range by which the intensity of the light changes
    [SerializeField] float waveDuration; // The time it takes to light up and dim down
    [SerializeField] float durationRandomizer; // The amount by which the duration is randomized
    [SerializeField] Vector3 axis; // The axis on which to apply the effect
    [SerializeField] bool RandomizeAxis; // Wether the axis can be inverted
    float timer; // A timer counting from -1 - 1 in the time it takes to light up
    float timeStep;
    Vector3 defaultAngle;
    bool left = false;
    Vector3 finalAxis;

    // Start is called before the first frame update
    void Start()
    { 
        defaultAngle = this.transform.localRotation.eulerAngles;
        timeStep = 2 / (waveDuration + Random.Range(-durationRandomizer, durationRandomizer));
        timer = -1;
        finalAxis = axis;
    }

    // Update is called once per frame
    void Update()
    {
        if (left)
        {
            timer -= timeStep * Time.deltaTime;
            if (timer <= -1)
            {
                left = false;
                timeStep = 2 / (waveDuration + Random.Range(-durationRandomizer, durationRandomizer));
                if (RandomizeAxis)
                {
                    Vector3 rand = new Vector3(Random.Range(-2, 2), Random.Range(-2, 2), Random.Range(-2, 2));
                    finalAxis = new Vector3(axis.x * rand.x, axis.y * rand.y, axis.z * rand.z);
                }
            }
        }
        else
        {
            timer += timeStep * Time.deltaTime;
            if (timer >= 1)
            {
                left = true;
                timeStep = 2 / (waveDuration + Random.Range(-durationRandomizer, durationRandomizer));
            }
        }

        Vector3 finalAngle = defaultAngle + (finalAxis * (timer * Intensity));
        if (RandomizeAxis)
        {
            this.transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(finalAngle), 0.2f);
        }
        else
        {
            this.transform.localRotation = Quaternion.Euler(finalAngle);
        }
        
    }
}
