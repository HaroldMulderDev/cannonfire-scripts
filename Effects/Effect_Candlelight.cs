﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect_Candlelight : MonoBehaviour
{
    [SerializeField] float CandleIntensityRange; // The range by which the intensity of the light changes
    [SerializeField] float brightenDuration; // The time it takes to light up and dim down
    [SerializeField] float durationRandomizer; // The amount by which the duration is randomized
    float timer; // A timer counting from -1 - 1 in the time it takes to light up
    float timeStep;
    float defaultIntensity;
    bool dimming = false;
    Light light;

    // Start is called before the first frame update
    void Start()
    {
        light = this.GetComponent<Light>();
        defaultIntensity = light.intensity;
        timeStep = 2 / (brightenDuration + Random.Range(-durationRandomizer, durationRandomizer));
        timer = -1; 
    }

    // Update is called once per frame
    void Update()
    {
        if (dimming)
        {
            timer -= timeStep * Time.deltaTime;
            if (timer <= -1)
            {
                dimming = false;
                timeStep = 2 / (brightenDuration + Random.Range(-durationRandomizer, durationRandomizer));
            }
        }
        else
        {
            timer += timeStep * Time.deltaTime;
            if (timer >= 1)
            {
                dimming = true;
                timeStep = 2 / (brightenDuration + Random.Range(-durationRandomizer, durationRandomizer));
            }
        }

        float finalIntensity = defaultIntensity + (timer * CandleIntensityRange);
        light.intensity = finalIntensity;
    }
}
