﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A line in 3d space, takes two points
/// </summary>
public class Line
{
    Vector3 st, nd;
    /// <summary>
    /// Line constructor
    /// </summary>
    public Line(Vector3 start, Vector3 end)
    {
        st = start;
        nd = end;
    }

    /// <summary>
    /// Calculates the position based of the point on the line
    /// </summary>
    /// <param name="positionOnLine">A 0-1 value of the position on the line</param>
    /// <returns>The position of the point on the line</returns>
    public Vector3 getPosition(float point) {
        point = Mathf.Clamp(point, 0, 1);
        Vector3 additional = (nd - st) * point;
        Vector3 pos = st + additional;
        return pos;
    }
}
