﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Events;
using UnityEngine.UI;

public class EditableField : MonoBehaviour
{
    [Header("Generic")]
    [SerializeField] string[] values;
    [SerializeField] float EditWait;
    [SerializeField] int amountOfIndexes;
    [Header("Profile")]
    [SerializeField] bool pullFromSkinList;
    [SerializeField] bool doProfileLoadOnFinish;
    [SerializeField] bool doUpdateSkinOnFinish;
    [SerializeField] int profileIndex;
    Profile profile;
    [Header("Pull: Directories")]
    [SerializeField] bool pullFromDirectoryList;
    [SerializeField] string directoryPathFromRoot;
    [Header("Audiomanagement")]
    [SerializeField] bool doManageAudio;
    [SerializeField] string audioGroup;
    Audiomanager audioManager;
    string[] items;
    Gamemanager gamemanager;
    public UnityEvent onDeselect;
    Text text;
    int[] indexes;
    int position;

    bool isSelected; // Wether we are editing this field
    bool canMakeEdit; // Wether there is any delays or other restrictions active which stop editing
    bool isRestricted; // Wether or not to restrict certain events, used when this wasn't setup correctly
    bool isInSetup = true; // Wether this is not ready yet and therefore cannot be displayed

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(lateStart(.25f));
    }

    IEnumerator lateStart(float time)
    {
        yield return new WaitForSeconds(time);
        if (doProfileLoadOnFinish && doUpdateSkinOnFinish)
        {
            Debug.LogWarning("Editable field was probably setup incorrectly, multiple profile fields assigned!");
            isRestricted = true;
        }

        indexes = new int[amountOfIndexes];
        text = this.GetComponent<Text>();
        gamemanager = Gamemanager.Instance;

        profile = gamemanager.profiles[profileIndex];

        if (doManageAudio)
        {
            audioManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<Audiomanager>();
            indexes[0] = 20;
            onDeselect.AddListener(attemptChangeAudio);
        }

        if (doProfileLoadOnFinish && !isRestricted)
        {
            onDeselect.AddListener(attemptProfileLoad);
        }

        if (doUpdateSkinOnFinish && !isRestricted)
        {
            onDeselect.AddListener(attemptSkinUpdate);
        }

       if (pullFromDirectoryList)
        {
            values = Directory.GetDirectories(FilePath.dataRoot + directoryPathFromRoot);
            for(int i = 0; i < values.Length; i++)
            {
                values[i] = values[i].Replace(FilePath.dataRoot + directoryPathFromRoot,"");
            }
            items = values;
            Debug.Log("Items:");
            foreach (string s in items)
            {
                Debug.Log(s);
            }
            isInSetup = false;
        }
        else if (pullFromSkinList)
        {
            StartCoroutine(loadAfterTime(0.5f));
        }
        else
        {
            isInSetup = false;
            items = values;
            Debug.Log("Items:");
            foreach (string s in items)
            {
                Debug.Log(s);
            }
        }
    }

    IEnumerator loadAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        items = gamemanager.skinManager.getValidSkinList(profile);
        Debug.Log("Items:");
        foreach (string s in items)
        {
            Debug.Log(s);
        }
        isInSetup = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isInSetup)
        {
            text.text = getIndexesToString();
        }
        else
        {
            return;
        }
        if (isSelected)
        {
            text.fontStyle = FontStyle.Bold;
        }
        else
        {
            text.fontStyle = FontStyle.Normal;
        }

        if (isSelected)
        {
            if (Input.GetButtonDown("Submit") && canMakeEdit)
            {
                isSelected = false;
                onDeselect.Invoke();
            }

            if (Input.GetAxisRaw("Vertical") > 0 && canMakeEdit)
            {
                //go up
                cycleIndex(1, position);
                StartCoroutine(waitForEdit(EditWait));

            }

            if (Input.GetAxisRaw("Vertical") < 0 && canMakeEdit)
            {
                //go down
                cycleIndex(-1, position);
                StartCoroutine(waitForEdit(EditWait));
            }

            if (Input.GetAxisRaw("Horizontal") < 0 && canMakeEdit)
            {
                //go left
                cyclePosition(-1);
                StartCoroutine(waitForEdit(EditWait));

            }

            if (Input.GetAxisRaw("Horizontal") > 0 && canMakeEdit)
            {
                //go right
                cyclePosition(1);
                StartCoroutine(waitForEdit(EditWait));
            }
        }
    }

    void cycleIndex(int movement, int position)
    {
        if(movement > 0)
        {
            while(movement > 0)
            {
                indexUp(position);
                movement--;
            }
        }
        else if(movement < 0)
        {
            while (movement < 0)
            {
                indexDown(position);
                movement++;
            }
        }
    }

    void cyclePosition(int movement)
    {
        if (movement > 0)
        {
            while (movement > 0)
            {
                positionUp();
                movement--;
            }
        }
        else if (movement < 0)
        {
            while (movement < 0)
            {
                positionDown();
                movement++;
            }
        }
    }

    void indexUp(int position)
    {
        if(indexes[position] + 1 > items.Length - 1)
        {
            indexes[position] = 0;
        }
        else
        {
            indexes[position]++;
        }
    }

    void indexDown(int position)
    {
        if (indexes[position] - 1 < 0)
        {
            indexes[position] = items.Length - 1;
        }
        else
        {
            indexes[position]--;
        }
    }

    void positionUp()
    {
        if (position + 1 > indexes.Length - 1)
        {
            position = 0;
        }
        else
        {
            position++;
        }
    }

    void positionDown()
    {
        if (position - 1 < 0)
        {
            position = indexes.Length - 1;
        }
        else
        {
            position--;
        }
    }

    public void Select()
    {
        isSelected = true;
        StartCoroutine(waitForEdit(EditWait));
    }

    IEnumerator waitForEdit(float time)
    {
        canMakeEdit = false;
        yield return new WaitForSeconds(time);
        canMakeEdit = true;
    }

    void attemptProfileLoad()
    {
        if(profile != null)
        {
            profile.Unload();
            profile.load(getIndexesToString(true));
        }
    }

    void attemptSkinUpdate()
    {
        if (profile != null)
        {
            profile.setSkin(getIndexesToString(true));
        }
    }

    void attemptChangeAudio()
    {
        if(audioManager != null)
        {
            switch (audioGroup)
            {
                case "Master":
                    audioManager.setMasterVolume(float.Parse(items[indexes[0]]) / 100);
                    break;
                case "Music":
                    audioManager.setMusicVolume(float.Parse(items[indexes[0]]) / 100);
                    break;
                case "SFX":
                    audioManager.setSFXVolume(float.Parse(items[indexes[0]]) / 100);
                    break;
                case "Ambient":
                    audioManager.setAmbientVolume(float.Parse(items[indexes[0]]) / 100);
                    break;
            }
        }
    }

    string getIndexesToString(bool raw = false) {
        string str = "";
        for(int i = 0; i < indexes.Length; i++)
        {
            if (i == position && isSelected && !raw)
            {
                str += "<color=#808080ff>";
                str += items[indexes[i]];
                str += "</color>";
            }
            else
            {
                Debug.Log(items[indexes[i]]);
                str += items[indexes[i]];
            }
        }
        return str;
    }
}
