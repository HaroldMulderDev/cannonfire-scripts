﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannonball : MonoBehaviour
{
    int damage; // The damage this deals when hitting from the most effective range
    float damageFalloff; // The falloff for damage based on distance. This is rounded to an integer after calculations
    [SerializeField] GameObject impactPref; // The prefab for the impact object
    [SerializeField] GameObject criticalImpactPref; // The prefab for the impact object
    [SerializeField] float maxLifetime; // The maximum time in seconds this will exist for
    [SerializeField] float shotMissCompensation; // The amount of charge that is returned when a shot misses
    public Ship origin;
    float lifeTime;
    bool hitTarget;
    Vector3 startingPosition;
    
    // Start is called before the first frame update
    void Start()
    {
        Gamemode gamemode = Gamemode.instance;
        damage = gamemode.properties.cannonballDamage;
        damageFalloff = gamemode.properties.cannonballDamageFalloff;
        lifeTime = 0;
        startingPosition = transform.position;
        Vector3 torque = new Vector3(Random.Range(-.02f, .02f), Random.Range(-.02f, .02f), Random.Range(-.02f, .02f));
        this.GetComponent<Rigidbody>().AddTorque(torque, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        lifeTime += Time.deltaTime;
        if(lifeTime > maxLifetime)
        {
            destroyBullet();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        CritBox critbox = collision.collider.GetComponent<CritBox>();
        if (critbox != null)
        {
            GameObject go = Instantiate(criticalImpactPref);
            //GameObject go = Instantiate(impactPref);
            //go.transform.position = this.transform.position;
            go.transform.position = this.transform.position;
            damageTarget(critbox);
            hitTarget = true;
            destroyBullet();
        }

        Ship ship = collision.gameObject.GetComponent<Ship>();
        if (ship != null)
        {
            GameObject go = Instantiate(impactPref);
            go.transform.position = this.transform.position;
            hitTarget = true;
            damageTarget(ship);
        }

        destroyBullet();
    }

    /// <summary>
    /// Calculates the damage to deal based on distance traveled
    /// </summary>
    /// <param name="distance">The distance in world space</param>
    /// <returns>Int: The final damage to deal</returns>
    int calculateDamage(float distance)
    {
        int falloff = Mathf.CeilToInt(damageFalloff * distance);
        int nettoDamage = damage - falloff;
        return nettoDamage;
    }

    /// <summary>
    /// Damages a ship based
    /// </summary>
    /// <param name="ship">The ship to damage</param>
    void damageTarget(Ship ship)
    {
        float dist = (startingPosition - transform.position).magnitude;
        int damage = calculateDamage(dist);
        Gamemode gamemode = Gamemode.instance;
        ship.damage(damage, origin.UID);
    }

    /// <summary>
    /// Damages a ship based
    /// </summary>
    /// <param name="critbox">The critbox of a ship to damage</param>
    void damageTarget(CritBox critbox)
    {
        float dist = (startingPosition - transform.position).magnitude;
        int damage = calculateDamage(dist);
        Gamemode gamemode = Gamemode.instance;
        critbox.damage(damage, origin.UID);
        //origin.rankPoints += critbox.damage(damage);
    }

    void destroyBullet()
    {
        if (!hitTarget)
        {
            origin.addCharge(shotMissCompensation);
        }
        Destroy(this.gameObject);
    }
}
