﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Settings
{
    public string version = "1.00";

    public float masterVolume = 0;
    public float musicVolume = 0;
    public float SFXVolume = 0;
    public float ambientVolume = 0;

    public bool masterVolumeOn = true;
    public bool musicVolumeOn = true;
    public bool SFXVolumeOn = true;
    public bool ambientVolumeOn = true;

    public Settings()
    {

    }
}
