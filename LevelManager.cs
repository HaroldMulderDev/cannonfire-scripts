﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class LevelManager : MonoBehaviour
{
    Gamemanager gameManager;
    [SerializeField] string[] regionIDs;
    [SerializeField] string[] scenes0;
    [SerializeField] string[] scenes1;
    [SerializeField] string[] scenes2;
    [SerializeField] string[] scenes3;
    [SerializeField] string[] scenes4;
    Dictionary<string, string[]> Regions;

    // Start is called before the first frame update
    void Start()
    {
        Regions = new Dictionary<string, string[]>();

        Regions.Add(regionIDs[0], scenes0);
        Regions.Add(regionIDs[1], scenes1);
        Regions.Add(regionIDs[2], scenes2);
        Regions.Add(regionIDs[3], scenes3);
        Regions.Add(regionIDs[4], scenes4);

        gameManager = GameObject.FindGameObjectWithTag("Gamemanager").GetComponent<Gamemanager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void loadLevel(string regionID = "Carribean")
    {
        if (Regions[regionID] != null)
        {
            if (Regions[regionID].Length > 0)
            {
                gameManager.onLevelLoad();
                int i = Random.Range(0, Regions[regionID].Length);
                Debug.Log("Loading scene...");
                Debug.Log(regionID);
                Debug.Log(Regions[regionID][i]);
                SceneManager.LoadScene(Regions[regionID][i], LoadSceneMode.Single);
            }
            else
            {
                Debug.LogError("No scenes in region!");
            }
        }
        else
        {
            Debug.LogError("Region does not exist!");
        }
    }

    public void loadCredits()
    {
        SceneManager.LoadScene("Credits", LoadSceneMode.Single);
    }
}
