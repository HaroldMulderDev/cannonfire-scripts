﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColoredHUDComponent : MonoBehaviour
{
    [SerializeField] ColoredHUDComponent provider;
    [SerializeField] Image[] images;
    [SerializeField] Text[] textElements;
    [SerializeField] public Color color;

    // Start is called before the first frame update
    void Start()
    {
        if(provider != null)
        {
            color = provider.color;
        }

        foreach (Image i in images)
        {
            i.color = color;
        }

        foreach (Text t in textElements)
        {
            t.color = color;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
