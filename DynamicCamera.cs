﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicCamera : MonoBehaviour
{
    [SerializeField] GameObject[] targets = new GameObject[2];
    [SerializeField] float minimumZoom; // The minimum zoom this can have
    [SerializeField] float maximumZoom; // The maximum zoom this can have
    [SerializeField] float zoomMultiplier; // how much the camera zooms based of the gameobjects distance.
    [SerializeField] float zoomBuffer; // how much the camera zooms based of the gameobjects distance.
    Vector3 standardAngle;

    // Start is called before the first frame update
    void Start()
    {
        standardAngle = transform.forward;    
    }

    // Update is called once per frame
    void Update()
    {
        if (targets[0] != null && targets[1] != null)
        {
            correctZoomLevel();
        }
    }

    void correctZoomLevel() {
        Vector3 centerPoint = targets[0].transform.position + ((targets[1].transform.position - targets[0].transform.position)/2);
        float distance = Vector3.Distance(targets[0].transform.position, targets[1].transform.position);
        float zoom = Mathf.Clamp((distance * zoomMultiplier) + zoomBuffer, minimumZoom, maximumZoom);
        Vector3 pos = centerPoint + (-standardAngle * zoom);
        this.transform.position = pos;
    }
}
