﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RammingNode : MonoBehaviour
{
    [SerializeField] Ship parentShip;
    [SerializeField] GameObject impactPref; // The prefab for the impact object
    [SerializeField] GameObject criticalImpactPref; // The prefab for the impact object
    float damagePerSpeed; // The damage this deals per speed unit
    [SerializeField] float minimalWaitTime; // The time in seconds between ramming
    float waitTime;
    public bool canRam;
    Gamemode gamemode;

    private void Start()
    {
        gamemode = Gamemode.instance;
        damagePerSpeed = gamemode.properties.rammingDamage;
    }

    private void Update()
    {
        if(waitTime > 0)
        {
            waitTime -= Time.deltaTime;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (waitTime <= 0 && canRam)
        {
            Debug.Log("collision detected!");

            CritBox critbox = collision.collider.GetComponent<CritBox>();
            if (critbox != null)
            {
                GameObject gc = Instantiate(criticalImpactPref);
                //GameObject go = Instantiate(impactPref);
                //go.transform.position = this.transform.position;
                gc.transform.position = this.transform.position;
                damageTarget(critbox);
            }

            Ship ship = collision.gameObject.GetComponent<Ship>();
            if (ship != null && ship != parentShip)
            {
                GameObject go = Instantiate(impactPref);
                go.transform.position = this.transform.position;
                damageTarget(ship);
            }
        }
    }

    /// <summary>
    /// Calculates the damage to deal based on ship speed
    /// </summary>
    /// <param name="distance">The distance in world space</param>
    /// <returns>Int: The final damage to deal</returns>
    int calculateDamage(float speed)
    {
        return Mathf.CeilToInt(damagePerSpeed * speed);
    }

    /// <summary>
    /// Damages a ship based
    /// </summary>
    /// <param name="ship">The ship to damage</param>
    void damageTarget(Ship ship)
    {
        Debug.Log("Rammed once");
        int damage = calculateDamage(parentShip.activeSpeed);
        ship.damage(damage, parentShip.UID);
        //parentShip.rankPoints += ship.damage(damage);
        parentShip.drainCharge();
        ship.drainCharge();
        waitTime = minimalWaitTime;
    }

    /// <summary>
    /// Damages a ship based
    /// </summary>
    /// <param name="ship">The ship to damage</param>
    void damageTarget(CritBox critbox)
    {
        Debug.Log("Rammed once");
        int damage = calculateDamage(parentShip.activeSpeed);
        critbox.damage(damage, parentShip.UID);
        parentShip.drainCharge();
        critbox.drainCharge();
        waitTime = minimalWaitTime;
    }
}
