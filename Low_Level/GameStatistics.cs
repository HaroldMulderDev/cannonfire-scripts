﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameStatistics : MonoBehaviour
{
    //Mainly used to call objective updates
    [Header("Callback events")]
    public UnityEvent onTimeUpdate = new UnityEvent();
    public UnityEvent onDamageUpdate = new UnityEvent();
    public UnityEvent onIncomingDamageUpdate = new UnityEvent();
    public UnityEvent onEntityDestroyTarget = new UnityEvent();
    public UnityEvent onEntityDestroyed = new UnityEvent();
    public UnityEvent onEntityMoving = new UnityEvent();
    public UnityEvent onPlayerAction = new UnityEvent();
    public UnityEvent onRankPointIncrease = new UnityEvent();
    
    //The list of tracked game statistics
    [Header("Stats")]
    // Global stats
    public float gameTime;
    // Entity stats
    public Dictionary<int, int> damageDealt = new Dictionary<int, int>(); // A dictionary of damage dealt per entity
    public Dictionary<int, int> damageTaken = new Dictionary<int, int>(); // A dictionary of damage taken per entity
    public Dictionary<int, int> killCount = new Dictionary<int, int>(); // A dictionary of kills made per entity
    public Dictionary<int, int> deathCount = new Dictionary<int, int>(); // A dictionary of deaths per entity
    public Dictionary<int, float> movement = new Dictionary<int, float>(); // A dictionary of distance moved per entity
    public Dictionary<int, int> actionsByEntity = new Dictionary<int, int>(); // A dictionary of actions done per entity
    public Dictionary<int, int> rankPointsByEntity = new Dictionary<int, int>(); // A dictionary of rank points accumulated per entity
    public Dictionary<int, float> timePlayed = new Dictionary<int, float>(); // A dictionary of rank points accumulated per entity

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        gameTime += Time.deltaTime;
        foreach(int key in timePlayed.Keys)
        {
            timePlayed[key] += Time.deltaTime;
        }
        onTimeUpdate.Invoke();
    }

    public IEnumerator waitAndRestart(UnityAction call, float time)
    {
        yield return new WaitForSeconds(time);
        call.Invoke();
    }

    public void increaseEntityDamageDealt(int entityUID, int damage)
    {
        if (!damageDealt.ContainsKey(entityUID))
        {
            damageDealt.Add(entityUID, damage);
        }
        else
        {
            damageDealt[entityUID] += damage;
        }
        onDamageUpdate.Invoke();
    }

    public void increaseEntityDamageTaken(int entityUID, int damage)
    {
        if (!damageTaken.ContainsKey(entityUID))
        {
            damageTaken.Add(entityUID, damage);
        }
        else
        {
            damageTaken[entityUID] += damage;
        }
        onIncomingDamageUpdate.Invoke();
    }

    public void increaseKillCount(int entityUID)
    {
        if (!killCount.ContainsKey(entityUID))
        {
            killCount.Add(entityUID, 1);
        }
        else
        {
            killCount[entityUID]++;
        }
        onEntityDestroyTarget.Invoke();
    }

    public void increaseDeathCount(int entityUID)
    {
        if (!deathCount.ContainsKey(entityUID))
        {
            deathCount.Add(entityUID, 1);
        }
        else
        {
            deathCount[entityUID]++;
        }
        onEntityDestroyed.Invoke();
    }

    public void increaseMovementDistance(int entityUID, float movement)
    {
        if (!this.movement.ContainsKey(entityUID))
        {
            this.movement.Add(entityUID, movement);
        }
        else
        {
            this.movement[entityUID] = movement;
        }
        onEntityMoving.Invoke();
    }

    public void increaseActions(int entityUID)
    {
        if (!actionsByEntity.ContainsKey(entityUID))
        {
            actionsByEntity.Add(entityUID, 1);
        }
        else
        {
            actionsByEntity[entityUID]++;
        }
        onPlayerAction.Invoke();
    }

    public void increaseRankPoints(int entityUID)
    {
        if (!rankPointsByEntity.ContainsKey(entityUID))
        {
            rankPointsByEntity.Add(entityUID, 1);
        }
        else
        {
            rankPointsByEntity[entityUID]++;
        }
        onRankPointIncrease.Invoke();
    }
}
