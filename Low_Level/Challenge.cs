﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reward
{
    int type = -1;
    int value = -1;
    SkinManager skinManager;

    public Reward skinReward(int skinIndex)
    {
        Reward r = new Reward(0, skinIndex);

        return r;
    }

    public Reward rankReward(int points)
    {
        Reward r = new Reward(1, points);

        return r;
    }

    public Reward(int type, int value)
    {
        this.type = type;
        this.value = value;
    }

    public void redeem(Profile profile)
    {
        switch (type)
        {
            case 0:
                // We are rewarding a skin
                skinManager = Gamemanager.Instance.GetComponent<Gamemanager>().skinManager;
                break;
            case 1:
                // We are rewarding a number of rank points
                profile.generic.rank += value;
                break;
        }
    }
}

public class Challenge
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
