﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Can create callbacks for advanced input types on request
/// </summary>
public class AdvancedInput : MonoBehaviour
{
    List<Callback> callbackEvents = new List<Callback>();
    GameObject callbackPref;
    GameObject callbackHolder;

    public AdvancedInput(GameObject callbackPref)
    {
        this.callbackPref = callbackPref;
        callbackHolder = GameObject.FindGameObjectWithTag("EmptyObjects");
    }

    public void createCallback(string ID, uint callbackType, KeyCode key)
    {
        GameObject go = Instantiate(callbackPref,callbackHolder.transform);
        Callback c = go.GetComponent<Callback>();
        c.setup(ID, callbackType, key);
        callbackEvents.Add(c);
    }

    public void subscribe(string ID, UnityAction func)
    {
        foreach(Callback c in callbackEvents)
        {
            if(c.ID == ID)
            {
                c.Subscribe(func);
                return;
            }
        }
        Debug.LogWarning("No callback was found with ID: " + ID);
    }

    public void cancel(string ID)
    {
        foreach (Callback c in callbackEvents)
        {
            if (c.ID == ID)
            {
                c.cancel();
                return;
            }
        }
        Debug.LogWarning("No callback was found with ID: " + ID);
    }
}
