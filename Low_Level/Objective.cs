﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct paramater
{
    public int type;
    public bool required;

    public static int INT{
        get { return 0; }
    }
    public static int FLOAT {
        get { return 1; }
    }
    public static int BOOLEAN {
        get { return 2; }
    }

}

public class Objective
{
    /*
     * Type list:
     * 0 = Ontimereached
     * 1 = OnDamageDealt
     * 2 = onKillsMade
     */

    /// <summary>
    /// Trigger = When this objective is triggered
    /// Repeat = The amount of times this can be triggered
    /// Delay = The amount of time that has to pass until this can trigger again
    /// OnActivate(Function); which listeners are added to the objective
    /// </summary>

    uint type;
    bool repeat;
    float delay;
    float value;
    public int entityID = -1; // Only used for entity specific objectives
    bool valueIsInteger; // Only used when we want integer value objectives
    public delegate void ObjectiveComplete();
    public delegate void EntityObjectiveComplete(int i);
    ObjectiveComplete objectiveComplete;
    EntityObjectiveComplete entityObjectiveComplete;
    GameStatistics stats;
    bool active = false;
    float startValue;

    public Objective(uint type, float value, bool repeat = false, float delay = 0, bool valueIsInteger = false)
    {
        this.type = type;
        this.value = value;
        this.repeat = repeat;
        this.delay = delay;
        this.valueIsInteger = valueIsInteger;
    }

    public void addListener(ObjectiveComplete listener)
    {
        objectiveComplete += listener;
    }

    public void addListener(EntityObjectiveComplete listener)
    {
        entityObjectiveComplete += listener;
    }

    public void Start(GameStatistics stats)
    {
        this.stats = stats;
        Restart();
    }

    void Restart()
    {
        active = true;
        switch (type)
        {
            case 0:
                stats.onTimeUpdate.AddListener(updateObjective);
                startValue = stats.gameTime;
                break;
            case 1:
                stats.onDamageUpdate.AddListener(updateObjective);
                if (stats.damageDealt.ContainsKey(entityID))
                {
                    if (delay > 0)
                    {
                        startValue = stats.damageDealt[entityID];
                    }
                    else
                    {
                        startValue = startValue + value;
                    }
                }
                else
                {
                    startValue = 0;
                }
                
                break;
            case 2:
                stats.onEntityDestroyTarget.AddListener(updateObjective);
                if (stats.killCount.ContainsKey(entityID))
                {
                    startValue = stats.killCount[entityID];
                }
                else
                {
                    startValue = 0;
                }

                break;
        }
    }

    float Value {
        get { return value; }
    }

    #region ObjectiveTypes

    public static Objective onTimeReached(float timeInSeconds, bool repeat)
    {

        Objective o = new Objective(0, timeInSeconds, repeat, 0, floatIsInteger(timeInSeconds));
        return o;
    }

    public static Objective onDamageDealt(float damage, int entity, float delay, bool repeat)
    {
        Objective o;
        if (floatIsInteger(damage))
        {
            o = new Objective(1, damage, repeat, delay, true);
            o.entityID = entity;

        }
        else
        {
            Debug.LogError("Damage can only be handled in integer values!");
            return null;
        }

        return o;
    }

    public static Objective onKillsMade(float kills, int entity, bool repeat)
    {
        Objective o;
        if (floatIsInteger(kills))
        {
            o = new Objective(2, kills, repeat, 0, true);
            o.entityID = entity;
        }
        else
        {
            Debug.LogError("Damage can only be handled in integer values!");
            return null;
        }

        return o;
    }

    #endregion

    /// <summary>
    /// Checks to see if a float value can be represented by an integer instead
    /// </summary>
    /// <param name="value">The value to check</param>
    /// <returns>Returns true if the value can be represented by an integer</returns>
    static bool floatIsInteger(float value)
    {
        bool isInteger = false;
        if (value == Mathf.RoundToInt(value))
        {
            isInteger = true;
        }
        return isInteger;
    }

    public void updateObjective()
    {
        if (active)
        {
            switch (type)
            {
                case 0:
                    checkTimeReached();
                    break;
                case 1:
                    checkIfDamageDealt();
                    break;
                case 2:
                    checkIfKillsMade();
                    break;
            }
        }
    }

    void callObjectiveEvent()
    {
        Debug.Log("An objective was completed!");
        objectiveComplete.Invoke();
        if(entityID >= 0)
        {
            entityObjectiveComplete.Invoke(entityID);
        }
        active = false;
        if (repeat)
        {
            restartAfter();
        }
    }

    void restartAfter()
    {
        stats.StartCoroutine(stats.waitAndRestart(Restart, delay));
    }

    #region Behavior specific

    void checkTimeReached()
    {
        if (stats.gameTime - startValue >= value)
        {
            // Objective was completed!
            Debug.Log("Time data:" + stats.gameTime + " - " + startValue + " >= " + value);
            callObjectiveEvent();
        }
    }

    void checkIfDamageDealt()
    {
        if (!stats.damageDealt.ContainsKey(entityID))
        {
            return;
        }
        if (stats.damageDealt[entityID] - startValue >= value)
        {
            // Objective was completed!
            Debug.Log("damage data:" + stats.damageDealt[entityID] + " - " + startValue + " >= " + value);
            callObjectiveEvent();
        }
    }

    void checkIfKillsMade()
    {
        if (!stats.killCount.ContainsKey(entityID))
        {
            return;
        }
        if (stats.killCount[entityID] - startValue >= value)
        {
            // Objective was completed!
            Debug.Log("kill data:" + stats.killCount[entityID] + " - " + startValue + " >= " + value);
            callObjectiveEvent();
        }
    }

    #endregion

    #region ParameterInfo

    public static bool getValidParameters(int type, string[] parameters)
    {
        paramater[] parameterList = getParameterListByType(type);
        Debug.Log("Parameters passed: ");
        foreach(string s in parameters)
        {
            Debug.Log(s);
        }
        Debug.Log("Parameters expected: ");
        foreach(paramater p in parameterList)
        {
            Debug.Log(p.type);
        }
        if(parameterList == null)
        {
            return false;
        }

        int length = 0;
        foreach(paramater p in parameterList)
        {
            if (p.required)
            {
                length++;
            }
        }

        if (parameters.Length >= length)
        {
            for (int i = 0; i < parameterList.Length; i++)
            {
                uint behavior = checkParam(parameters[i], parameterList[i].type, parameterList[i].required);
                switch (behavior)
                {
                    case 0:
                        return false;
                        break;
                    case 1:
                        // This is the only way to use a parameter!
                        break;
                    case 2:
                        return false;
                        break;
                }
            }    
        }
        else
        {
            Debug.Log("Not enough paramaters to support objective");
            return false;
        }

        return true;
    }

    /// <summary>
    /// Checks a signle parameters and determines if it is useful
    /// </summary>
    /// <param name="param">The parameter to check</param>
    /// <param name="type">The type to convert to (0=int,1=float,2=bool)</param>
    /// <param name="required">Wether this is a required parameter</param>
    /// <returns>0 if parameter is invalid, 1 if parameter is valid, 2 if parameter should be defaulted</returns>
    static uint checkParam(string param, int type, bool required)
    {
        //Debug.Log("Param: " + param + " - " + type + " - " + required);
        switch (type)
        {
            case 0:
                //Integer
                int i;
                if (int.TryParse(param, out i))
                {
                    return 1;
                }
                else
                {
                    if (!required)
                    {
                        return 2;
                    }
                }
                break;
            case 1:
                //Float
                float ii;
                if (float.TryParse(param, out ii))
                {
                    return 1;
                }
                else
                {
                    if (!required)
                    {
                        return 2;
                    }
                }
                break;
            case 2:
                //Boolean
                bool val;
                if (bool.TryParse(param, out val))
                {
                    return 1;
                }
                else
                {
                    if (!required)
                    {
                        return 2;
                    }
                }
                break;
        }

        return 0;
    }

    static paramater[] getParameterListByType(int type)
    {
        paramater[] paramaters;
        switch (type)
        {
            case 0:
                paramaters = new paramater[2];
                paramaters[0] = new paramater();
                paramaters[0].type = paramater.FLOAT;
                paramaters[0].required = true;
                paramaters[1] = new paramater();
                paramaters[1].type = paramater.BOOLEAN;
                paramaters[1].required = true;
                return paramaters;
                break;
            case 1:
                paramaters = new paramater[4];
                paramaters[0] = new paramater();
                paramaters[0].type = paramater.FLOAT;
                paramaters[0].required = true;
                paramaters[1] = new paramater();
                paramaters[1].type = paramater.INT;
                paramaters[1].required = true;
                paramaters[2] = new paramater();
                paramaters[2].type = paramater.FLOAT;
                paramaters[2].required = true;
                paramaters[3] = new paramater();
                paramaters[3].type = paramater.BOOLEAN;
                paramaters[3].required = true;
                return paramaters;
                break;
            case 2:
                paramaters = new paramater[3];
                paramaters[0] = new paramater();
                paramaters[0].type = paramater.FLOAT;
                paramaters[0].required = true;
                paramaters[1] = new paramater();
                paramaters[1].type = paramater.INT;
                paramaters[1].required = true;
                paramaters[2] = new paramater();
                paramaters[2].type = paramater.BOOLEAN;
                paramaters[2].required = true;
                return paramaters;
                break;
        }
        return null;
    }

    #endregion
}
