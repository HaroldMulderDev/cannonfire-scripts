﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action
{
    //Action types:
    // 1 = EndMatch
    // 2 = selectWinnerByKills
    int type; // What we are looking to do
    bool active = false;
    GameStatistics stats;
    Gamemode gamemode;
    Gamemanager gamemanager;

    public Action(int type)
    {
        this.type = type;
        gamemanager = Gamemanager.Instance;
    }

    public void Start(Gamemode gamemode)
    {
        this.stats = gamemode.stats;
        this.gamemode = gamemode;
        active = true;
    }

    public void execute(int entityID = -1)
    {
        if (active)
        {
            switch (type)
            {
                case 0:
                    EndMatch();
                    break;
                case 1:
                    selectWinnerByKills();
                    break;
                case 2:
                    setWinner(entityID);
                    break;
                case 3:
                    awardRankPoint(entityID);
                    break;
            }
        }
    }

    #region actionTypes

    public static Action SelectWinnerByKills()
    {
        Action act = new Action(1);
        return act;
    }

    public static Action SetWinner()
    {
        Action act = new Action(2);
        return act;
    }

    public static Action AwardRankPoint()
    {
        Action act = new Action(3);
        return act;
    }

    #endregion

    #region Type specific behavior

    void EndMatch()
    {
        //

    }

    void selectWinnerByKills()
    {
        // Determine winner profile
        int[] winningEntity = new int[2];
        winningEntity[0] = -1;
        winningEntity[1] = -1;
        foreach (KeyValuePair<int, int> kv in stats.killCount)
        {
            if (gamemode.getEntity(kv.Key) == null)
            {
                Debug.Log("entity not found in gamemode");
            }
            else
            {
                if (gamemode.getEntity(kv.Key).representingProfile != null)
                {
                    // This is in fact a player
                    if (kv.Value > winningEntity[1])
                    {
                        // The last was lower or equal
                        winningEntity[0] = kv.Key;
                        winningEntity[1] = kv.Value;
                    }
                }
            }
        }
        if(winningEntity[0] != -1)
        {
            // Gamemanager display winning player profile
            gamemanager.winner = gamemode.getEntity(winningEntity[0]).representingProfile;
        }
    }

    void setWinner(int entityID)
    {
        //Debug.Log("Winner ID: " + entityID);
        gamemanager.winner = gamemode.getEntity(entityID).representingProfile;
        //Debug.Log(gamemode.getEntity(entityID).representingProfile);
    }

    void awardRankPoint(int entityID)
    {
        gamemode.onRankPoint.Invoke(entityID);
    }

    #endregion
}
