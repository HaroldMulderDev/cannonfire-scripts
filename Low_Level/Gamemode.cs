﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using System.IO;

[Serializable]
public class ModeProperties
{
    // Game toggles
    public bool doShipRespawning = false;
    public bool doShipBouncing = true;
    public bool doGameTimer = false;

    // Ship settings
    public int shipHealth = 3200;
    public int cannonballDamage = 30;
    public float cannonballDamageFalloff = 0.25f;
    public int rammingDamage = 10;
    public float criticalHitMultiplier = 1.3f;

    // Match settings
    public float MatchTime = 300;

    // Trackers
    public bool trackKills = false;
    public bool trackDamage = false;
    public bool trackSpeed = false;
    public bool trackRankPoints = false;
}

public class IntegerEvent : UnityEvent<int>
{

}

public class DoubleIntegerEvent : UnityEvent<int,int>
{

}

public class IntFloatEvent : UnityEvent<int, float>
{

}

public class Gamemode : MonoBehaviour
{
    public static Gamemode instance;

    [SerializeField] string gamemode; // The current gamemode, the string should be the name of the gamemode path/folder

    List<Entity> entities;
    bool isRanked; // wether or not this match is ranked, custom gamemodes should not be able to be ranked!
    public DoubleIntegerEvent onDamageDealt; // Should be triggered when a ship deals damage to a target, int: the attacker
    public DoubleIntegerEvent onDamageTaken; // Should be triggered when a ship takes damage, int: the damage taker
    public IntegerEvent onDestroyTarget; // Should be triggered when a target is destroyed, int: the destroyer
    public IntegerEvent onDestroyed; // Should be triggered when a ship is destroyed, int: the destroyed ship
    public IntegerEvent onEntityAction; // Should be triggered whenever an entity is given an action by a player
    public IntFloatEvent onEntityMove; // Should be triggered during a frame whenever an entity moved in world space
    public IntegerEvent onRankPoint; // Should be triggered during a frame whenever an entity moved in world space

    public GameStatistics stats;
    public ModeProperties properties;

    Dictionary<string, Objective> objectives;
    [SerializeField] string[] entityEventActions;
    [SerializeField] string[] entityTypes;
    List<Spawnpoint> spawnpoints;

    //Per entity stat trackers, Will be enabled based on modeProperties
    statTracker[] trackers;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        stats = this.gameObject.GetComponent<GameStatistics>();
        entities = new List<Entity>();
        onDamageDealt = new DoubleIntegerEvent();
        onDamageTaken = new DoubleIntegerEvent();
        onDestroyTarget = new IntegerEvent();
        onDestroyed = new IntegerEvent();
        onEntityMove = new IntFloatEvent();
        onEntityAction = new IntegerEvent();
        onRankPoint = new IntegerEvent();

        objectives = new Dictionary<string, Objective>();

        onDamageDealt.AddListener(stats.increaseEntityDamageDealt);
        onDamageTaken.AddListener(stats.increaseEntityDamageTaken);
        onDestroyTarget.AddListener(stats.increaseKillCount);
        onDestroyed.AddListener(stats.increaseDeathCount);
        onEntityMove.AddListener(stats.increaseMovementDistance);
        onEntityAction.AddListener(stats.increaseActions);
        onRankPoint.AddListener(stats.increaseRankPoints);

        string json = JSONInterface.readFile("settings.json", FilePath.gamemodes + gamemode + "\\", false);
        properties = JsonUtility.FromJson<ModeProperties>(json);
        string evAc = JSONInterface.readFile("Entity_Event_Actions.txt", FilePath.gamemodes + gamemode + "\\", false);
        entityEventActions = evAc.Split('\n');
    }

    public void AddTracker(statTracker tracker)
    {
        switch (tracker.statType)
        {
            case 0:
                if (!properties.trackDamage)
                {
                    tracker.gameObject.SetActive(false);
                }
                break;
            case 1:
                if (!properties.trackKills)
                {
                    tracker.gameObject.SetActive(false);
                }
                break;
            case 2:
                if (!properties.trackSpeed)
                {
                    tracker.gameObject.SetActive(false);
                }
                break;
            case 3:
                if (!properties.trackRankPoints)
                {
                    tracker.gameObject.SetActive(false);
                }
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void addEntity(Entity entity)
    {
        entities.Add(entity);
        //stats.damageDealt.Add(getEntityIndex(entity), 0);

        // For each entity, event-action, add an objective
        // Event action provided: "InternalName,0,5,0,false;0"
        foreach (string eventAction in entityEventActions)
        {
            //Debug.Log("here!");
            string[] seperate = eventAction.Split(';'); // Seperate our event objective and action
            // Setup objective
            string[] objective = seperate[0].Split(','); // Seperate our parameters
            string[] parameters = new string[objective.Length - 2];
            string objectiveID = "NAN";
            for (int i = 0; i < objective.Length; i++)
            {
                if(i >= 2)
                {
                    parameters[i - 2] = objective[i];
                }
            }

            int type;
            if (!int.TryParse(objective[1], out type))
            {
                Debug.LogError("Objective type invalid...");
                break;
            }

            objectiveID = objective[0] + "_" + getEntityIndex(entity);
            if (findExistingObjective(objectiveID) == null)
            {
                switch (type)
                {
                    case 1:
                        string[] actualParamaters = new string[parameters.Length + 1];
                        for (int i = 0; i < actualParamaters.Length; i++)
                        {
                            if (i == 1)
                            {
                                actualParamaters[1] = getEntityIndex(entity).ToString();
                            }
                            else if (i > 1)
                            {
                                actualParamaters[i] = parameters[i - 1];
                            }
                            else
                            {
                                actualParamaters[i] = parameters[i];
                            }

                        }
                        if (Objective.getValidParameters(type, actualParamaters))
                        {
                            Debug.Log("Created Objective for entity: #" + getEntityIndex(entity));
                            Debug.Log("Parameter data: " + parameters);
                            Objective obj = Objective.onDamageDealt(float.Parse(parameters[0]), getEntityIndex(entity), float.Parse(parameters[1]), bool.Parse(parameters[2]));
                            objectives.Add(objectiveID, obj);
                            obj.addListener(logObjectiveWasDone);
                            obj.Start(stats);
                        }
                        else
                        {
                            Debug.LogError("Parameter mismatch detected! Cannot create objective");
                        }
                        break;
                    case 2:
                        actualParamaters = new string[parameters.Length + 1];
                        for (int i = 0; i < actualParamaters.Length; i++)
                        {
                            if (i == 1)
                            {
                                actualParamaters[1] = getEntityIndex(entity).ToString();
                            }
                            else if (i > 1)
                            {
                                actualParamaters[i] = parameters[i - 1];
                            }
                            else
                            {
                                actualParamaters[i] = parameters[i];
                            }

                        }
                        if (Objective.getValidParameters(type, actualParamaters))
                        {
                            Debug.Log("Created Objective for entity: #" + getEntityIndex(entity));
                            Debug.Log("Parameter data: " + parameters);
                            Objective obj = Objective.onKillsMade(float.Parse(parameters[0]), getEntityIndex(entity), bool.Parse(parameters[1]));
                            objectives.Add(objectiveID, obj);
                            obj.addListener(logObjectiveWasDone);
                            obj.Start(stats);
                        }
                        else
                        {
                            Debug.LogError("Parameter mismatch detected! Cannot create objective");
                        }
                        break;
                }
            }

            // Add action to the objective
            if(findExistingObjective(objectiveID) != null)
            {
                string[] action = seperate[1].Split(','); // Seperate our parameters

                int actionType;
                if (!int.TryParse(action[0], out actionType))
                {
                    Debug.LogError("Action type invalid...");
                    break;
                }
                Action act;
                switch (actionType)
                {
                    case 1:
                        act = Action.SelectWinnerByKills();
                        act.Start(this);
                        objectives[objectiveID].addListener(act.execute);
                        break;
                    case 2:
                        act = Action.SetWinner();
                        act.Start(this);
                        objectives[objectiveID].addListener(act.execute);
                        break;
                    case 3:
                        act = Action.AwardRankPoint();
                        act.Start(this);
                        objectives[objectiveID].addListener(act.execute);
                        break;
                    default:
                        Debug.Log("Action doesn't exist or is not an entity action!");
                        break;
                }
            }
        }
    }

    Objective findExistingObjective(string name)
    {
        foreach(KeyValuePair<string, Objective> kv in objectives)
        {
            if(kv.Key == name)
            {
                // Objective already exists
                return kv.Value;
            }
        }
        return null;
    }

    void logObjectiveWasDone()
    {
        Debug.Log("Objective was completed!");
    }

    public int getEntityIDOfProfile(Profile profile)
    {
        foreach(Entity ent in entities)
        {
            if(ent.representingProfile == profile)
            {
                return ent.UID;
            }
        }
        return -1;
    }

    public Entity getEntity(int index)
    {
        if (index >= 0 && index < entities.Count)
        {
            if (entities[index] != null)
            {
                return entities[index];
            }
        }
        Debug.LogWarning("Couldn't find entity!");
        return null;
    }

    public int getEntityIndex(Entity entity)
    {
        if (entities.Contains(entity))
        {
            for(int i = 0; i < entities.Count; i++)
            {
                if(entities[i] == entity)
                {
                    return i;
                }
            }
        }
        Debug.LogWarning("Couldn't find entity!");
        return -1;
    }
}
