﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Callback : MonoBehaviour
{
    public UnityEvent triggerEvent;
    public string ID;
    public uint type;
    public KeyCode key;
    // Local data
    float timeSinceLastPress;
    bool isDoubledTapped;

    /// <summary>
    /// Callback constructor
    /// </summary>
    /// <param name="ID">The identifier name of this callback</param>
    /// <param name="type">The event this callback checks for</param>
    /// <param name="key">The key which to check</param>
    public void setup(string ID, uint type, KeyCode key)
    {
        this.ID = ID;
        this.type = type;
        this.key = key;
        triggerEvent = new UnityEvent();
    }

    public void Subscribe(UnityAction func)
    {
        triggerEvent.AddListener(func);
    }

    public void cancel()
    {
        isDoubledTapped = false;
    }

    private void Update()
    {
        timeSinceLastPress += Time.deltaTime;
        // Only care about the last press when we start pressing, then double tap is true until released
        if (Input.GetKeyDown(key) && timeSinceLastPress < 0.35f)
        {
            isDoubledTapped = true;
            if (type == doubleTapDown)
            {
                // trigger point
                triggerEvent.Invoke();
            }
        }

        if (Input.GetKeyDown(key))
        {
            timeSinceLastPress = 0;
        }

        if (Input.GetKeyUp(key))
        {
            if (isDoubledTapped && type == doubleTapUp)
            {
                // Trigger point
                triggerEvent.Invoke();
            }

            isDoubledTapped = false;
        }

        if (Input.GetKey(key))
        {
            if (isDoubledTapped && type == doubleTap)
            {
                //trigger point
                triggerEvent.Invoke();
            }
        }
    }

    /// <summary>
    /// Callback is active when the user double taps and holds a key
    /// </summary>
    public static uint doubleTap {
        get { return 0; }
    }

    /// <summary>
    /// The callback is active the first frame the user double taps the key
    /// </summary>
    public static uint doubleTapDown {
        get { return 1; }
    }

    /// <summary>
    /// The callback is active the frame the user releases the key after having double tapped
    /// </summary>
    public static uint doubleTapUp {
        get { return 2; }
    }
}
