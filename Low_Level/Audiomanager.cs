﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Audiomanager : MonoBehaviour
{
    Gamemanager gameManager;
    [SerializeField] AudioMixer audioMixer;
    [SerializeField] float lowerAudioBoundary;
    [SerializeField] float upperAudioBoundary;
    float audioRange;
    public static Audiomanager _instance = null;

    // Start is called before the first frame update
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        
        DontDestroyOnLoad(this.gameObject);
        gameManager = GameObject.FindGameObjectWithTag("Gamemanager").GetComponent<Gamemanager>();
        gameManager.updateAllSettings.AddListener(updateAudio);
        gameManager.updateAudioSettings.AddListener(updateAudio);
        audioRange = upperAudioBoundary - lowerAudioBoundary;
    }

    void updateAudio()
    {
        float masterVolume = Mathf.Clamp(gameManager.settings.masterVolume, lowerAudioBoundary, upperAudioBoundary);
        float musicVolume = Mathf.Clamp(gameManager.settings.musicVolume, lowerAudioBoundary, upperAudioBoundary);
        float SFXVolume = Mathf.Clamp(gameManager.settings.SFXVolume, lowerAudioBoundary, upperAudioBoundary);
        float ambientVolume = Mathf.Clamp(gameManager.settings.ambientVolume, lowerAudioBoundary, upperAudioBoundary);

        if (!gameManager.settings.masterVolumeOn)
        {
            masterVolume = -80;
        }

        if (!gameManager.settings.musicVolumeOn)
        {
            musicVolume = -80;
        }

        if (!gameManager.settings.SFXVolumeOn)
        {
            SFXVolume = -80;
        }

        if (!gameManager.settings.ambientVolumeOn)
        {
            ambientVolume = -80;
        }

        audioMixer.SetFloat("MasterVolume", masterVolume);
        audioMixer.SetFloat("MusicVolume", musicVolume);
        audioMixer.SetFloat("SFXVolume", SFXVolume);
        audioMixer.SetFloat("AmbientVolume", ambientVolume);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region Advanced getters/setters
    //---------------
    // Master
    //---------------

    /// <summary>
    /// Sets the master volume
    /// </summary>
    /// <param name="value">A value between 0-1 indicating volume</param>
    public void setMasterVolume(float value)
    {
        float final = 0;
        if(value == 0)
        {
            gameManager.settings.masterVolumeOn = false;
        }
        else
        {
            gameManager.settings.masterVolumeOn = true;
        }
        
        final = lowerAudioBoundary + (value * audioRange);

        gameManager.settings.masterVolume = final;
        gameManager.updateAudioSettings.Invoke();
    }

    /// <summary>
    /// Returns the value of the master volume
    /// </summary>
    /// <param name="percentile">when true, this returns a value from 0-1</param>
    /// <returns></returns>
    public float getMasterVolume(bool percentile)
    {
        float final = 0;
        if (percentile)
        {
            final = 1 + (gameManager.settings.masterVolume / audioRange);
        }
        else
        {
            if (gameManager.settings.masterVolumeOn) {
                final = gameManager.settings.masterVolume;
            }
            else
            {
                final = -80;
            }
        }

        return final;
    }

    //---------------
    // Music
    //---------------

    /// <summary>
    /// Sets the music volume
    /// </summary>
    /// <param name="value">A value between 0-1 indicating volume</param>
    public void setMusicVolume(float value)
    {
        float final = 0;
        if (value == 0)
        {
            gameManager.settings.musicVolumeOn = false;
        }
        else
        {
            gameManager.settings.musicVolumeOn = true;
        }

        final = lowerAudioBoundary + (value * audioRange);

        gameManager.settings.musicVolume = final;
        gameManager.updateAudioSettings.Invoke();
    }

    /// <summary>
    /// Returns the value of the music volume
    /// </summary>
    /// <param name="percentile">when true, this returns a value from 0-1</param>
    /// <returns></returns>
    public float getMusicVolume(bool percentile)
    {
        float final = 0;
        if (percentile)
        {
            final = 1 + (gameManager.settings.musicVolume / audioRange);
        }
        else
        {
            if (gameManager.settings.musicVolumeOn)
            {
                final = gameManager.settings.musicVolume;
            }
            else
            {
                final = -80;
            }
        }

        return final;
    }

    //---------------
    // SFX
    //---------------

    /// <summary>
    /// Sets the SFX volume
    /// </summary>
    /// <param name="value">A value between 0-1 indicating volume</param>
    public void setSFXVolume(float value)
    {
        float final = 0;
        if (value == 0)
        {
            gameManager.settings.SFXVolumeOn = false;
        }
        else
        {
            gameManager.settings.SFXVolumeOn = true;
        }

        final = lowerAudioBoundary + (value * audioRange);

        gameManager.settings.SFXVolume = final;
        gameManager.updateAudioSettings.Invoke();
    }

    /// <summary>
    /// Returns the value of the SFX volume
    /// </summary>
    /// <param name="percentile">when true, this returns a value from 0-1</param>
    /// <returns></returns>
    public float getSFXVolume(bool percentile)
    {
        float final = 0;
        if (percentile)
        {
            final = 1 + (gameManager.settings.SFXVolume / audioRange);
        }
        else
        {
            if (gameManager.settings.SFXVolumeOn)
            {
                final = gameManager.settings.SFXVolume;
            }
            else
            {
                final = -80;
            }
        }

        return final;
    }

    //---------------
    // Ambience
    //---------------

    /// <summary>
    /// Sets the ambient volume
    /// </summary>
    /// <param name="value">A value between 0-1 indicating volume</param>
    public void setAmbientVolume(float value)
    {
        float final = 0;
        if (value == 0)
        {
            gameManager.settings.ambientVolumeOn = false;
        }
        else
        {
            gameManager.settings.ambientVolumeOn = true;
        }

        final = lowerAudioBoundary + (value * audioRange);

        gameManager.settings.ambientVolume = final;
        gameManager.updateAudioSettings.Invoke();
    }

    /// <summary>
    /// Returns the value of the ambient volume
    /// </summary>
    /// <param name="percentile">when true, this returns a value from 0-1</param>
    /// <returns></returns>
    public float getAmbientVolume(bool percentile)
    {
        float final = 0;
        if (percentile)
        {
            final = 1 + (gameManager.settings.ambientVolume / audioRange);
        }
        else
        {
            if (gameManager.settings.ambientVolumeOn)
            {
                final = gameManager.settings.ambientVolume;
            }
            else
            {
                final = -80;
            }
        }

        return final;
    }
    #endregion
}
