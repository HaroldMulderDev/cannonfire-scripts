﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Mathemetics
{
    /// <summary>
    /// Returns an exponential value from a non-exponental value
    /// </summary>
    /// <param name="normal"></param>
    /// <param name="exponant"></param>
    /// <returns></returns>
    static public float toExponential(float normal, uint exponant)
    {
        int count = (int)exponant;
        float factor;
        if (count < 0)
        {
            count = -count;
        }
        factor = multiplyRecursively(normal, normal, count);
        
        //Debug.Log(factor);
        return factor;
    }

    static float multiplyRecursively(float value, float multiplier, int count)
    {
        float ret = value;
        for(int i = 0; i < count; i++)
        {
            ret *= multiplier;
        }
        return ret;
    }

    static public float sign(float value)
    {
        if(value > 0)
        {
            return 1;
        }
        else if (value < 0)
        {
            return -1;
        }
        return 0;
    }

    static public int sign(int value)
    {
        if (value > 0)
        {
            return 1;
        }
        else if (value < 0)
        {
            return -1;
        }
        return 0;
    }
}
