﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

static public class FilePath
{
    static public string general;
    static public string dataRoot;
    static public string profiles;
    static public string gamemodes;
}

public class JSONInterface
{
    public JSONInterface()
    {
        FilePath.general = "general\\";
        FilePath.profiles = "profiles\\";
        FilePath.gamemodes = "gamemodes\\";
        FilePath.dataRoot = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "\\.cannonfire_buccaneers\\";

        if (!Directory.Exists(FilePath.dataRoot))
        {
            Debug.Log("setting up required folders...");
            JSONInterface.createDirectories();
        }
    }

    static public string readFile(string fileName, string pathFromRoot, bool useDataRoot = true)
    {
        if (useDataRoot)
        {
            return File.ReadAllText(FilePath.dataRoot + pathFromRoot + fileName);
        }
        return File.ReadAllText(pathFromRoot + fileName);

    }

    static public void createJSON(string json, string fileName, string pathFromRoot)
    {
        FileStream fileStream = File.Open(FilePath.dataRoot + pathFromRoot + fileName + ".json", FileMode.OpenOrCreate);
        fileStream.Flush();
        fileStream.Close();
        File.WriteAllText(FilePath.dataRoot + pathFromRoot + fileName + ".json", json);
    }

    static public void createDirectories()
    {
        Directory.CreateDirectory(FilePath.dataRoot);
        Directory.CreateDirectory(FilePath.dataRoot + FilePath.general);
        Directory.CreateDirectory(FilePath.dataRoot + FilePath.profiles);
        Directory.CreateDirectory(FilePath.dataRoot + FilePath.gamemodes);
    }
}
