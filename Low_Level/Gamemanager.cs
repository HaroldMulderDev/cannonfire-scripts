﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Gamemanager : MonoBehaviour
{
    [Header("Ingame")]
    [SerializeField] public bool isIngame;
    public bool isRankedGame;
    public Profile[] profiles = new Profile[2];
    public Profile winner;
    [SerializeField] Ship blue;
    [SerializeField] Ship orange;
    [SerializeField] Text winnerDisplay;
    [SerializeField] float endOfGameWaitTime;
    bool blueWins = false;
    bool orangeWins = false;
    public Settings settings;
    public UnityEvent updateAllSettings;
    public UnityEvent updateAudioSettings;
    JSONInterface jsonInterface;
    [SerializeField] GameObject gamemodePref;
    [SerializeField] GameObject skinManagerPref;
    static Gamemanager instance;
    public Gamemode gamemode;
    public SkinManager skinManager;

    float winGamePause = 0.5f; // A short time to wait before ending in order to allow callbacks to go through
    public static Gamemanager Instance
    {
        get {
            if (instance == null)
            {
                instance = GameObject.FindGameObjectWithTag("Gamemanager").GetComponent<Gamemanager>();
            }
            return instance;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        jsonInterface = new JSONInterface();
        gamemode = Instantiate(gamemodePref).GetComponent<Gamemode>();
        skinManager = Instantiate(skinManagerPref).GetComponent<SkinManager>();

        if (Directory.Exists(FilePath.dataRoot + FilePath.general))
        {
            if(File.Exists(FilePath.dataRoot + FilePath.general + "settings.json"))
            {
                loadSettings();
            }
            else
            {
                Debug.Log("No settings json found... creating new one!");
                createNewSettings();
                loadSettings();
            }
        }
        else
        {
            Debug.LogError("Folder data not correctly setup! cannot find path: " + FilePath.dataRoot + FilePath.general);
        }


        updateAllSettings.Invoke();
        StartCoroutine(lateStart(0.5f));
    }

    IEnumerator lateStart(float time)
    {
        yield return new WaitForSeconds(time);
        if (isIngame && isRankedGame)
        {
            profiles[0].generic.rank = Mathf.Clamp(profiles[0].generic.rank - 3, 0, 1500);
            profiles[1].generic.rank = Mathf.Clamp(profiles[1].generic.rank - 3, 0, 1500);
            Debug.Log("Blue rank after match cost: " + profiles[0].generic.rank + " CP");
            Debug.Log("Orange rank after match cost: " + profiles[1].generic.rank + " CP");
        }
        else if (isIngame)
        {
            blue.representingProfile = profiles[0];
            orange.representingProfile = profiles[1];
        }
    }

    void createNewSettings()
    {
        Settings set = new Settings();

        set.SFXVolume = 0;
        set.musicVolume = 0;
        set.masterVolume = 0;

        string json = JsonUtility.ToJson(set);
        Debug.Log("JSON data: " + json);
        JSONInterface.createJSON(json, "settings", FilePath.general);
    }

    

    void saveSettings()
    {
        string json = JsonUtility.ToJson(settings);
        Debug.Log("JSON data: " + json);
        JSONInterface.createJSON(json, "settings", FilePath.general);
    }

    void loadSettings()
    {
        Debug.Log("Loading settings... ");
        string json = JSONInterface.readFile("settings.json", FilePath.general);
        settings = JsonUtility.FromJson<Settings>(json);
    }

    public void onLevelLoad()
    {
        saveSettings();
    }

    // Update is called once per frame
    void Update()
    {
        if (isIngame)
        {
            if (!blueWins && !orangeWins) {
                if (orange.health <= 0)
                {
                    blueWins = true;
                    blue.rankPoints++;
                }
                if (blue.health <= 0)
                {
                    orangeWins = true;
                    orange.rankPoints++;
                }
            }

            if (winner != null && winGamePause <= 0)
            {
                winnerDisplay.text = winner.generic.name + " Wins!";
                winnerDisplay.gameObject.SetActive(true);
                if(endOfGameWaitTime > 0)
                {
                    endOfGameWaitTime -= Time.deltaTime;
                }
                else
                {
                    if (isRankedGame)
                    {
                        profiles[0].generic.rank = Mathf.Clamp(profiles[0].generic.rank + (orange.rankWorth * blue.rankPoints), 0, 1500);
                        profiles[1].generic.rank = Mathf.Clamp(profiles[1].generic.rank + (blue.rankWorth * orange.rankPoints), 0, 1500);
                        Debug.Log("Blue rank after match: " + profiles[0].generic.rank + " CP");
                        Debug.Log("Orange rank after match: " + profiles[1].generic.rank + " CP");
                        profiles[0].Save();
                        profiles[1].Save();
                    }

                    updateStatistics();
                    SceneManager.LoadScene("UI", LoadSceneMode.Single);
                }
            }
            else if(winner != null)
            {
                winGamePause -= Time.deltaTime;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                foreach(Profile p in profiles)
                {
                    p.Save();
                }
            }
        }
    }

    void updateStatistics()
    {
        GameStatistics gameStats = gamemode.stats;
        foreach (Profile prof in profiles)
        {
            int ID = gamemode.getEntityIDOfProfile(prof);
            // Update all statistics
            prof.statistics.GamesPlayed++;
            if (gameStats.damageDealt.ContainsKey(ID))
            {
                prof.statistics.Damage += gameStats.damageDealt[ID];
            }
            if (gameStats.killCount.ContainsKey(ID))
            {
                prof.statistics.Kills += gameStats.killCount[ID];
            }
            if (gameStats.movement.ContainsKey(ID))
            {
                prof.statistics.DistanceTravelled += gameStats.movement[ID];
            }
            if (gameStats.timePlayed.ContainsKey(ID))
            {
                prof.statistics.TimePlayed += gameStats.timePlayed[ID];
            }
            if (gameStats.actionsByEntity.ContainsKey(ID))
            {
                prof.statistics.InputsDone += gameStats.actionsByEntity[ID];
            }

            // Check for hit progression targets

            
            // Reward skins when applicable

            
        }
    }

    private void OnDestroy()
    {
        saveSettings();
    }
}
