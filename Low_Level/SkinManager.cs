﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinManager : MonoBehaviour
{
    [SerializeField] GameObject[] SkinsPrefs;
    Dictionary<string, GameObject> skins;

    // Start is called before the first frame update
    void Start()
    {
        skins = new Dictionary<string, GameObject>();
        for(int i = 0; i < SkinsPrefs.Length; i++)
        {
            skins.Add(SkinsPrefs[i].name, SkinsPrefs[i]);
            Debug.Log("Possible skin: " + SkinsPrefs[i].name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject getSkin(string skinName)
    {
        if (skins.ContainsKey(skinName))
        {
            return Instantiate(skins[skinName]);
        }
        return null;
    }

    public string[] getValidSkinList(Profile profile)
    {
        List<string> skinsHolder = new List<string>();
        skinsHolder.Add("default");

        string name;
        // Check all skin save data
        // Regional
        if (profile.skins.Caribbean)
        {
            name = "carribean";
            if (this.skins.ContainsKey(name))
            {
                skinsHolder.Add(name);
            }
            else
            {
                Debug.LogWarning("Invalid skin name, does not exist in dictionary!");
            }
        }
        if (profile.skins.Barataria)
        {
            name = "barataria";
            if (this.skins.ContainsKey(name))
            {
                skinsHolder.Add(name);
            }
            else
            {
                Debug.LogWarning("Invalid skin name, does not exist in dictionary!");
            }
        }
        if (profile.skins.Clewbay)
        {
            name = "clewbay";
            if (this.skins.ContainsKey(name))
            {
                skinsHolder.Add(name);
            }
            else
            {
                Debug.LogWarning("Invalid skin name, does not exist in dictionary!");
            }
        }
        if (profile.skins.Maries)
        {
            name = "maries";
            if (this.skins.ContainsKey(name))
            {
                skinsHolder.Add(name);
            }
            else
            {
                Debug.LogWarning("Invalid skin name, does not exist in dictionary!");
            }
        }
        if (profile.skins.Phillipines)
        {
            name = "phillipines";
            if (this.skins.ContainsKey(name))
            {
                skinsHolder.Add(name);
            }
            else
            {
                Debug.LogWarning("Invalid skin name, does not exist in dictionary!");
            }
        }

        // Rank Tier
        if (profile.skins.DarkAura)
        {
            name = "darkAura";
            if (this.skins.ContainsKey(name))
            {
                skinsHolder.Add(name);
            }
            else
            {
                Debug.LogWarning("Invalid skin name, does not exist in dictionary!");
            }
        }

        // return valid array
        string[] skins = new string[skinsHolder.Count];
        for(int i = 0; i < skinsHolder.Count; i++)
        {
            skins[i] = skinsHolder[i];
        }
        return skins;
    }
}
