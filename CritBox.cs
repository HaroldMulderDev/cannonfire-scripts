﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An object allowing modified damage to an entity.
/// </summary>
public class CritBox : MonoBehaviour
{
    [SerializeField] public Ship ship; // The ship this is a part of
    public float multiplier;

    // Start is called before the first frame update
    void Start()
    {
        Gamemode gamemode = Gamemode.instance;
        multiplier = gamemode.properties.criticalHitMultiplier;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int damage(int amount, int UID)
    {
        int finalDamage = Mathf.CeilToInt((float)amount * multiplier);
        if(finalDamage > 0)
        {
            Debug.Log("Crititcal hit onto: " + ship);
            return ship.damage(finalDamage, UID);
        }
        return 0;
    }

    public void drainCharge()
    {
        ship.drainCharge();
    }
}
