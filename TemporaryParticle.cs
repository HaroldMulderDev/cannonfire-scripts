﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporaryParticle : MonoBehaviour
{
    private ParticleSystem ps;
    [SerializeField] bool isChild; // Set to true if the particlesystem is a child of the bigger effect object. This will remove its parent instead.
    [SerializeField] bool isInteractable; // Set to true if this is aparticle that will have to be removed using the Interactables manager

    public void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    public void Update()
    {
        if (ps)
        {
            if (!ps.IsAlive())
            {
                if (isChild)
                {
                    destroy(transform.parent.gameObject);
                }
                destroy(gameObject);
            }
        }
    }

    void destroy(GameObject go)
    {
        Destroy(go);
    }
}
