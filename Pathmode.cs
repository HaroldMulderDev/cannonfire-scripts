﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathmode
{
    uint mode = 0;

    public Pathmode(uint mode = 0)
    {
        this.mode = mode;
    }


    #region Getters
    /// <summary>
    /// Matches the position of the node while mantaining rotation values
    /// </summary>
    public static Pathmode positionOnly {
        get {
            Pathmode p = new Pathmode(0);
            return p;
        }
    }

    /// <summary>
    /// Matches the rotation of the node while mantaining position values
    /// </summary>
    public static Pathmode rotationOnly {
        get {
            Pathmode p = new Pathmode(1);
            return p;
        }
    }

    /// <summary>
    /// Matches both position and rotation of the node
    /// </summary>
    public static Pathmode moveAndRotate {
        get {
            Pathmode p = new Pathmode(2);
            return p;
        }
    }
    #endregion
    #region functions
    /// <summary>
    /// Returns the updated position and rotation based on the pathmode
    /// </summary>
    /// <param name="originalPosition">The location from which the object moved</param>
    /// <param name="originalRotation">The rotation the object had before moving</param>
    /// <param name="node">The transform of the target node</param>
    /// <param name="mover">The transform of moving object node</param>
    /// <param name="interpolation">The interpolation step between 0-1</param>
    /// <returns>The updated transform values</returns>
    public void move(Vector3 originalPosition, Quaternion originalRotation, Transform node, Transform mover, float interpolation)
    {
        Vector3 newPos = originalPosition;
        Quaternion newRot = originalRotation;

        switch (mode)
        {
            default:
                Debug.LogWarning("Pathmode not found!");
                break;
            case 0:
                // Move only
                newPos = Vector3.Lerp(originalPosition, node.position, interpolation);
                break;
            case 1:
                // Move only
                newRot = Quaternion.Lerp(originalRotation, node.rotation, interpolation);
                break;
            case 2:
                // Move only
                newPos = Vector3.Lerp(originalPosition, node.position, interpolation);
                newRot = Quaternion.Lerp(originalRotation, node.rotation, interpolation);
                break;
        }

        mover.position = newPos;
        mover.rotation = newRot;
    }
    #endregion
}
