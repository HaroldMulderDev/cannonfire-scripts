﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.Events;

public class Profile : MonoBehaviour
{
    Gamemanager gamemanager;
    public GenericProfileData generic;
    public StatisticsProfileData statistics;
    public ProgressionProfileData progression;
    public SkinsProfileData skins;
    public static bool[] profileSlotsTaken = new bool[2];
    [SerializeField] int profileIndex;
    bool isLoaded = false;
    GameObject currentSkin;
    public GameObject CurrentSkin{
        get { return currentSkin; }
    }
    public UnityEvent onLoad;
    public UnityEvent onSkinUpdate;

    public bool IsLoaded {
        get { return isLoaded; }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (profileSlotsTaken[profileIndex] == true)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            profileSlotsTaken[profileIndex] = true;
            if (gamemanager == null)
            {
                gamemanager = GameObject.FindGameObjectWithTag("Gamemanager").GetComponent<Gamemanager>();
                gamemanager.profiles[profileIndex] = this;
            }
            if(profileIndex == 0)
            {
                if (!load("defaultProfile01"))
                {
                    create("defaultProfile01");
                    load("defaultProfile01");
                }
            }
            else if(profileIndex == 1)
            {
                if (!load("defaultProfile02"))
                {
                    create("defaultProfile02");
                    load("defaultProfile02");
                }
            }
            setSkin("default");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gamemanager == null)
        {
            gamemanager = Gamemanager.Instance;
            gamemanager.profiles[profileIndex] = this;
        }
    }

    void Create(string name)
    {
        if (!Directory.Exists(FilePath.dataRoot + FilePath.profiles + name + "\\"))
        {
            Directory.CreateDirectory(FilePath.dataRoot + FilePath.profiles + name + "\\");
        }

        //Generic
        GenericProfileData _generic = new GenericProfileData();
        _generic.name = name;
        _generic.rank = 0;
        _generic.isCheating = false;

        string json = JsonUtility.ToJson(_generic);
        Debug.Log("JSON data: " + json);
        JSONInterface.createJSON(json, "profile", FilePath.profiles + name + "\\");
        
        //stats
        StatisticsProfileData _statistics = new StatisticsProfileData();
        _statistics.GamesPlayed = 0;

        string json2 = JsonUtility.ToJson(_statistics);
        Debug.Log("JSON data: " + json2);
        JSONInterface.createJSON(json2, "statistics", FilePath.profiles + name + "\\");


        //progression
        ProgressionProfileData _progression = new ProgressionProfileData();
        _progression.achievementsUnlocked = 0;

        string json3 = JsonUtility.ToJson(_progression);
        Debug.Log("JSON data: " + json3);
        JSONInterface.createJSON(json3, "progression", FilePath.profiles + name + "\\");

        //skins
        SkinsProfileData _skins = new SkinsProfileData();   

        string json4 = JsonUtility.ToJson(_skins);
        Debug.Log("JSON data: " + json4);
        JSONInterface.createJSON(json4, "skins", FilePath.profiles + name + "\\");
    }

    public bool load(string name)
    {
        if (Directory.Exists(FilePath.dataRoot + FilePath.profiles + name + "\\"))
        {
            Debug.Log("Loading Profile: " + name + "...");
            string json = File.ReadAllText(FilePath.dataRoot + FilePath.profiles + name + "\\" + "profile.json");
            generic = JsonUtility.FromJson<GenericProfileData>(json);

            json = File.ReadAllText(FilePath.dataRoot + FilePath.profiles + name + "\\" + "statistics.json");
            statistics = JsonUtility.FromJson<StatisticsProfileData>(json);

            json = File.ReadAllText(FilePath.dataRoot + FilePath.profiles + name + "\\" + "progression.json");
            progression = JsonUtility.FromJson<ProgressionProfileData>(json);

            json = File.ReadAllText(FilePath.dataRoot + FilePath.profiles + name + "\\" + "skins.json");
            skins = JsonUtility.FromJson<SkinsProfileData>(json);

            Debug.Log(generic.name);
            Debug.Log(generic.rank + " CP");
            Debug.Log(statistics.GamesPlayed + " games played!");
            Debug.Log(progression.achievementsUnlocked + " achievements unlocked!");
            isLoaded = true;
            onLoad.Invoke();
            return true;
        }
            return false;
    }

    public void create(string name)
    {
        Directory.CreateDirectory(FilePath.dataRoot + FilePath.profiles + name + "\\");
        generic = new GenericProfileData();
        generic.name = name;
        statistics = new StatisticsProfileData();
        progression = new ProgressionProfileData();
        skins = new SkinsProfileData();
        Save();
    }

    public void Save()
    {
        string json = JsonUtility.ToJson(generic);
        JSONInterface.createJSON(json, "profile", FilePath.profiles + generic.name + "\\");
        json = JsonUtility.ToJson(statistics);
        JSONInterface.createJSON(json, "statistics", FilePath.profiles + generic.name + "\\");
        json = JsonUtility.ToJson(progression);
        JSONInterface.createJSON(json, "progression", FilePath.profiles + generic.name + "\\");
        json = JsonUtility.ToJson(skins);
        JSONInterface.createJSON(json, "skins", FilePath.profiles + generic.name + "\\");
    }

    public void Unload()
    {
        Save();
        generic = null;
        statistics = null;
        progression = null;
        skins = null;
        isLoaded = false;
    }

    public void setSkin(string skinName)
    {
        GameObject skin = gamemanager.skinManager.getSkin(skinName);
        if (skin != null)
        {
            Destroy(currentSkin);
            skins.SelectedSkin = skinName;
            DontDestroyOnLoad(skin);
            currentSkin = skin;
            onSkinUpdate.Invoke();
        }
        else
        {
            //Debug.Log("Something wen't terribly wrong here!");
            //Debug.Log(skinName);
        }
    }

}

[Serializable]
public class GenericProfileData
{
    public string name;
    public int rank;
    public bool isCheating;

    public GenericProfileData()
    {

    }
}

[Serializable]
public class StatisticsProfileData
{
    public int GamesPlayed = 0;

    public int Damage = 0;

    public int Kills = 0;

    public float DistanceTravelled = 0;
    public float TimePlayed = 0;
    public float InputsDone = 0;

    public StatisticsProfileData()
    {

    }
}

[Serializable]
public class ProgressionProfileData
{
    public int achievementsUnlocked;

    public int AchievementsUnlocked {
        get { return achievementsUnlocked; }
        set { achievementsUnlocked = value; }
    }

    public ProgressionProfileData()
    {

    }
}

[Serializable]
public class SkinsProfileData
{
    public string SelectedSkin = "default";

    public bool Caribbean = true;
    public bool Barataria = true;
    public bool Clewbay = true;
    public bool Maries = false;
    public bool Phillipines = true;
    public bool DarkAura = true;


    public SkinsProfileData()
    {

    }
}