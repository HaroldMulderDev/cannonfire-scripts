﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileItem : MonoBehaviour
{
    [SerializeField] Profile provider;
    [SerializeField] Text nameDisplay;
    [SerializeField] RankScoreBar rankDisplay;
    
    // Start is called before the first frame update
    void Start()
    {
        provider.onLoad.AddListener(changeValues);
        StartCoroutine(lateStart(1));
    }

    IEnumerator lateStart(float time)
    {
        yield return new WaitForSeconds(time);
        changeValues();
    }

    public void changeValues()
    {
        nameDisplay.text = provider.generic.name;
        rankDisplay.setValue = provider.generic.rank;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
