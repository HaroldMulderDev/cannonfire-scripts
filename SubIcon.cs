﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubIcon : MonoBehaviour
{
    [SerializeField] Image filledImage;
    [SerializeField] Image[] displayedImages;
    [SerializeField] GameObject activationEffect;
    [SerializeField] float activationEffectPitch = 1;
    [SerializeField] float value;
    bool isActivated = false;

    // Start is called before the first frame update
    void Start()
    {
        if (filledImage.fillAmount >= value)
        {
            isActivated = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(filledImage.fillAmount >= value)
        {
            if (!isActivated)
            {
                if (activationEffect != null) {
                    GameObject go = Instantiate(activationEffect, transform.position, transform.rotation);
                    go.GetComponent<AudioSource>().pitch = activationEffectPitch;
                }
            }
            isActivated = true;
        }
        else
        {
            isActivated = false;
        }

        if(isActivated)
        {
            foreach(Image img in displayedImages)
            {
                img.enabled = true;
            }
        }
        else
        {
            foreach (Image img in displayedImages)
            {
                img.enabled = false;
            }
        }
    }
}
