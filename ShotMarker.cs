﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotMarker : MonoBehaviour
{
    public Color color;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Renderer>().material.color = color;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
